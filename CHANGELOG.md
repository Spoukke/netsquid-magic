CHANGELOG
=========

2022-12-29 (v.13.3.0)
---------------------
- Added `success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory`.

2022-12-29 (v.13.2.3)
---------------------
- `PerfectStateSamplerFactory`, `DepolariseStateSamplerFactory`, `DepolariseWithFailureStateSamplerFactory` and `BitflipStateSamplerFactory` now return the correct Bell-state labels.

2022-12-15 (v.13.2.2)
---------------------
- Fixed bug in `MagicDistributor.abort_delivery` (label deliveries were not always successfully aborted).

2022-12-10 (v.13.2.1)
---------------------
- When aborting a delivery in `MagicDistributor` after the state was delivered but before the label was delivered now prevents the label from being delivered.
- Fixed bug in `MagicDistributor.abort_delivery` (state deliveries were not always successfully aborted).

2022-11-03 (v.13.2.0)
---------------------
- Added `AbstractHeraldedConnection` and `AbstractHeraldedDistributor` to magically simulate an abstracted version of heralded entanglement generation.

2022-10-26 (v.13.1.3)
---------------------
- Fixed the `get_label` method of `MagicDistributor` to also work after a state was delivered using an archiving system.
- `MagicDistributor` no longer always gives warnings when parameters are extracted from a component, only if those parameters overwrite other parameters.

2022-09-20 (v.13.1.2)
--------------------
- Bright-state parameter can now be either Python builtin float or numpy float (would lead to error before).

2022-08-02 (v.13.1.1)
---------------------
- Added the possibility to pass keyword arguments to `SingleClickMagicDistributor` and `DoubleClickMagicDistributor` to fix pipelining issues.

2022-01-19 (v.13.1.0)
---------------------
- Added coincidence probabilities as optional parameters to `DoubleClickMagicDistributor`.
- changes to `docs/Makefile` to move contents to common `Makefile`
- adapted CI to allow for new proxy server
- added deploy command for documentation to `setup.py`

2021-10-27 (v.13.0.0)
---------------------
- Added support for asymmetric bright-state parameters (alpha) in `SingleClickMagicDistributor`. This is a breaking change in how bright-state parameters are obtained from nodes.
- Added single-click model with number-resolving detectors
- Fixed a bug in `SingleClickMagicDistributor`, where `fixed_delivery_parameters` were being overwritten with `None` when a request for an entangled pair was placed.
- Updated parameter passing to `SingleClickMagicDistributor` to make it consistent with `DoubleClickMagicDistributor`.

2021-09-29 (v.12.1.0)
---------------------
- Added `DepolariseWithFailureMagicDistributor` for heralded entanglement distribution with Werner states.

2021-08-24 (v.12.0.0)
---------------------
- changed `HeraldedStateDeliverySamplerFactory` to only require a single function `func_delivery` that outputs a tuple `(state_sampler, success_probability)` to improve and simplify custom label assignment.
- Added argument `num_resolving` to `SingleClickDeliverySamplerFactory`. Currently, only `num_resolving=False` is implemented (previously, this was assumed implicitly).

2021-06-17 (v.11.0.1)
---------------------
- Fixed a bug in how `collection_efficiency` of a node was used to calculate `p_loss_init` in `SingleClickMagicDistributor` and `DoubleClickMagicDistributor`.

2021-06-02 (v.11.0.0)
---------------------
- move `_create_label_message` from `SingleClickMagicDistributor` to `HeraldedMagicDistributor` and simplify retrieving Bell indices from the label

2021-04-21 (v.10.0.0)
---------------------
- `SingleClickMagicDistributor` now reads out the bright-state parameter (alpha) from the nodes
- `DoubleClickMagicDistributor` now supports asymmetric links and multiplexing.

2021-03-16 (v.9.0.0)
--------------------
- updated to NetSquid v.1.0.6

2021-01-15 (v.8.0.0)
--------------------
- removed email addresses of contributors (except for the maintainer) from README
- undone the skipping of the example that was broken before but works now (see below at v.7.0.0)
- switched from using DataProtocol (removed from NetSquid) to Protocol
- enabled the `SingleClickMagicDistributor` to read its parameters from a `HeraldedConnection`
- made `SingleClickMagicDistributor` and `DoubleClickMagicDistributor` inherit from new class `HeraldedConnectionMagicDistributor`

2020-11-10 (v.7.0.0)
--------------------
- updated README
- Added multiplexing to DoubleClickMagicDistributor.
- skip broken example when running `make examples` (will be repaired as soon as `in_use` flag is available in NetSquid's QuantumProcessor)
- QuantumProcessors are now no longer busy while waiting for entanglement from MagicDistributor, but memory positions are set to `in_use=True`.

2020-09-17 (v.6.0.1)
--------------------
- replaced previously missed occurances of `qlink_interface.BellState` by `netsquid.BellIndex`.

2020-09-16 (v.6.0.0)
--------------------
- replace use of `qlink_interface.BellState` by `netsquid.BellIndex`

2020-09-16 (v.5.3.0)
--------------------
- Removed restriction for merging magic distributors that the IDs of the involved nodes are unique
- Added DoubleClickMagicDistributor to emulate heralded entanglement generation using a double-click protocol. Only supports symmetric links.
- fixed variable name (exampledir) in `Makefile`, so that examples are now also run by the pipeline
- updated to latest version of physlayer (`bsm_detector` was renamed to detectors there)

2020-07-27 (v.5.2.0)
--------------------
Two changes that oddly enough did not propagate through previous merges (see below). The changes are necessary for the tests to work.
- renamed `MagicDistributor._state_deliveries to MagicDistributor._state_node_deliveries` in its `__init__` function
- renamed `MagicDistributor._label_deliveries to MagicDistributor._label_node_deliveries` in its `__init__` function

2020-07-27 (v.5.1.0)
--------------------
- Allowed for different state and label delays for different nodes in `MagicDistributor`.
- Updated requirement for netsquid-physlayer (had <1.0.0, but current master is 2.0.0 and does not break tests, therefore changed to <3.0.0).
- Bug fix: made MagicDistributor only wait for events coming from 'self'.
- Added test of `MagicDistributor._apply_noise`.
- `MagicDistributor._apply_noise` now gets `quantum_memory` and `positions` as arguments. 
- Entity and EventType imported from pydynaa instead of netsquid.

2020-07-17 (v5.0.1)
-------------------
- bug fix: probabilities with which random basis in magic link layer is chosen

2020-07-06 (v5.0.0)
-------------------
- in the magic link layer code, assert that a node has QuantumProcessor
- Magic link layer protocol now uses new version of `qlink_interface` (breaking change).

2020-07-01 (v4.0.2)
-------------------
- Added MagicLinkLayerWithSignalling, which is the same as the MagicLinkLayer, but using signalling (feature of netsquid protocols) to react.

2020-06-25 (v4.0.1)
-------------------
- Added two subclasses of MagicDistributor, and corresponding subclasses of IStateDeliverySamplerFactory: `DepolariseMagicDistributor` and `BitflipMagicDistributor`

2020-06-25 (v4.0.0)
-------------------
- Quantum processor is now busy during magic entanglement generation. This is done by executing a program with two instructions, where the first puts the sample state on the memory and the second applies noise from the magic distributor (for example NV dephasing noise).
- The link layer no longer applies correction based on midpoint outcome but instead returns what Bell pair was generated. This is provided by the magic distributor which now needs to implement the method `get_bell_state`
- add a small delay to when the state delivery is handled to make sure that this is after the processor is finished. This is done by multiplying the `total_state_delay` with 1 + 1e-10.
- Added warning then `tot_state_delay` is zero

2020-05-19 (v3.0.0)
-------------------
- Changes to measure directly functionality in magic link layer protocol with random bases:
  * One can now specify the random basis using either the enum directly or the value of it.
  * Small shift in how probability distributions are specified to allow for uniform distribution.
- The `namedtuple`s and `enum`s defining the link layer service is now moved to the seperate package `qlink-interface`.

2020-05-04 (v2.1.0)
-------------------
- `link_layer.MagicLinkLayerProtocol` can now handle when nodes are out of memory and wait until there is a free qubit. Waiting is done with the new class `sleeper.Sleeper`.

2020-04-30 (v2.0.0)
-------------------
- Functionality of `MagicDistributorAdaptor` is now included in `MagicDistributor` for simplification of classes. (Therefore, `MagicDistributor` now supports adding delivery when one of the nodes calls for it or when two nodes file a request with the same parameters.)
- One instance of `MagicDistributor` can now also hold information about multiple magic distributors simultaneously, i.e. the distributors can be "merged".

2020-04-29 (v1.6.2)
-------------------
- updated README by specifying maintainer of this snippet

2020-03-25 (v1.6.1)
-------------------
- renamed requesttypes

2020-03-24 (v1.6.0)
-------------------
- removed small errors in single-click state model
- added documentation to that model, also to indicate where it differs from its source in the literature

2020-03-18 (v1.5.0)
-------------------
- Updated link layer and added perfect state magic distributor
- Fixed deprecation warning for netsquid.sim_utils
- Allow for netsquid 0.8

2020-01-22 (v1.4.2)
-------------------
- Allow for netsquid 0.7

2020-01-20 (v1.4.1)
-------------------
- Fixed small bug in link layer magic.
- `MagicDistributor` can now be specified to also sample `None` states and therefore not skip rounds.
  Default is to skip rounds, so backwards compatible.

2019-12-18 (v1.4.0)
-------------------
- `HeraldedStateDeliverySamplerFactory` now supports custom labels for when a success is heralded. 
  These labels are passed on from `MagicDistributor` and default to integer labels when no custom labels are provided.
- The `MagicDistributor` is now capable of putting states of different sizes onto memories.

2019-11-13 (v1.3.0)
-------------------
- MagicDistributor can now (optionally) put the label of the sampled state on a port and one can specify a delay of 
  both when the state is put on the memory and when the message is delivered on the port.

2019-11-04 (v1.2.2)
-------------------
- State delivery sampler now samples from non-negative integers 0,1,2,.. instead of 1,2,3..

2019-11-04 (v1.2.1)
-------------------
- Now using netsquid 0.6.2 and netsquid-physlayer 0.1.3.

2019-09-31 (v1.2.0)
-------------------
- Added `magic_distributor_adapter`.

2019-09-30 (v1.1.1)
-------------------
- Small refactor.

2019-09-04
----------
- Renamed `std_total_phase` in the `SingleClickDeliverySamplerFactory` to `coherent_phase`, in 
  order to distinguish it from the standard deviation of the interferometric drift, which
  applies a dephasing channel.

2019-06-28
----------

- Created this snippet

PYTHON3      = python3
SOURCEDIR    = netsquid_magic
TESTDIR      = tests
EXAMPLES     = examples
RUNEXAMPLES  = ${EXAMPLES}/run_examples.py
PIP_FLAGS    = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org
MINCOV       = 80
DOCS_DIR     = docs
BUILDDIR     = docs/build

help:
	@echo "install           Installs the package (editable)."
	@echo "verify            Verifies the installation, runs the linter and tests."
	@echo "tests             Runs the tests (excluding slow ones)."
	@echo "tests_full        Runs the full tests (including slow ones)."
	@echo "examples          Runs the examples and makes sure they work."
	@echo "open-cov-report   Creates and opens the coverage report."
	@echo "lint              Runs the linter."
	@echo "deploy-bdist      Builds and uploads the package to the netsquid pypi server."
	@echo "bdist             Builds the package."
	@echo "build-docs        Builds the documentation."
	@echo "open-docs         Opens the documentation."
	@echo "docs         	 Builds and opens the documentation."
	@echo "deploy-docs       Builds and uploads the documentation to the documentation server."
	@echo "test-deps         Installs the requirements needed for running tests and linter."
	@echo "python-deps       Installs the requirements needed for using the package."
	@echo "docs-deps         Installs the requirements needed for building the documentation."
	@echo "clean             Removes all .pyc files."


test-deps:
	@echo -e "\n*** Installing test requirements for Snippet $(SOURCEDIR)"
	@$(PYTHON3) -m pip install -r test_requirements.txt

python-deps: _check_variables
	@echo -e "\n*** Installing package requirements for Snippet $(SOURCEDIR)"
	@echo -e "*** Requires the environment variables NETSQUIDPYPI_USER and NETSQUIDPYPI_PWD to be set"
	@$(PYTHON3) -m pip install -r requirements.txt ${PIP_FLAGS}

clean:
	@/usr/bin/find . -name '*.pyc' -delete

lint:
	@echo -e "\n*** Checking that Snippet $(SOURCEDIR) conforms to PEP8 coding style"
	@$(PYTHON3) -m flake8 ${SOURCEDIR} ${TESTDIR} ${EXAMPLES}

tests:
	@echo -e "\n*** Running unit tests for Snippet $(SOURCEDIR) (excluding slow tests)"
	@$(PYTHON3) -m pytest --cov=${SOURCEDIR} --cov-fail-under=${MINCOV} -m "not slow" tests

tests_full:
	@echo -e "\n*** Running unit tests for Snippet $(SOURCEDIR) (including slow tests)"
	@$(PYTHON3) -m pytest --cov=${SOURCEDIR} --cov-fail-under=${MINCOV} tests

open-cov-report:
	@echo -e "\n*** Generating coverage report of the unit and integration tests of Snippet $(SOURCEDIR)"
	@$(PYTHON3) -m pytest --cov=${SOURCEDIR} --cov-report html tests && xdg-open htmlcov/index.html

examples:
	@echo -e "\n*** Running examples of Snippet $(SOURCEDIR)"
	@${PYTHON3} ${RUNEXAMPLES} > /dev/null && echo "Examples OK!"

bdist:
	@echo -e "\n*** Create a binary Snippet $(SOURCEDIR) wheel distribution file"
	@$(PYTHON3) setup.py bdist_wheel

install: _check_variables test-deps
	@echo -e "\n*** Installing Snippet $(SOURCEDIR) package locally"
	@$(PYTHON3) -m pip install -e . ${PIP_FLAGS}

docs-deps: _check_variables
	@echo -e "\n*** Installing docs requirements for Snippet $(SOURCEDIR)"
	@$(PYTHON3) -m pip install -r $(DOCS_DIR)/requirements.txt ${PIP_FLAGS}

build-docs: docs-deps
	@echo -e "\n*** Making html documentation for nippet $(SOURCEDIR)"
	$(PYTHON3) -msphinx -M html $(DOCS_DIR)/ $(BUILDDIR)/

open-docs:
ifeq (, $(shell which open))
	xdg-open ${BUILDDIR}/html/index.html
else
	open ${BUILDDIR}/html/index.html
endif

docs: build-docs open-docs

deploy-docs: python-deps build-docs
	@echo -e "\n*** Uploading docs to documentation server (requires authentication)."
	@$(PYTHON3) setup.py deploy_docs

_check_variables:
ifndef NETSQUIDPYPI_USER
	$(error Set the environment variable NETSQUIDPYPI_USER before uploading)
endif
ifndef NETSQUIDPYPI_PWD
	$(error Set the environment variable NETSQUIDPYPI_PWD before uploading)
endif

_clean_dist:
	@/bin/rm -rf dist

deploy-bdist: _clean_dist bdist
	@echo -e "\n*** Deploying binary Snippet $(SOURCEDIR) wheel distribution file to server"
	@$(PYTHON3) setup.py deploy

verify: clean test-deps python-deps lint tests_full examples _verified

_verified:
	@echo "The Snippet $(SOURCEDIR) is verified :)"

.PHONY: clean lint test-deps python-deps tests tests_full verify bdist deploy-bdist _clean_dist install open-cov-report examples _check_variables docs-deps build-docs open-docs docs

[![pipeline status](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-magic/badges/master/pipeline.svg)](https://gitlab..nl/softwarequtech/netsquid-snippets/netsquid-magic/commits/master) [![coverage report](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-magic/badges/master/coverage.svg)](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-magic/commits/master)



NetSquid-Magic (13.3.0)
======================

Description
-----------
Tools for generating states between remote nodes by ‘magic’, i.e. from an analytical model or existing simulation data, without simulating every attempt.

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```

The most recent documentation of the master branch can be found [here](https://docs.netsquid.org/snippets/netsquid-magic/).

Usage
-----

See each individual [module](https://docs.netsquid.org/snippets/netsquid-magic/api.html) for how to use the various components and models.

Contact
-------

For questions, please contact the maintainer: Guus Avis (g.avis@tudelft.nl).

Contributors
------------
In alphabetical order:

- Guus Avis
- Tim Coopmans
- Axel Dahlberg
- Hana Jirovská
- David Maier
- Julian Rabbie
- Matthew Skrzypczyk

License
-------

The NetSquid-SnippetTemplate has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.

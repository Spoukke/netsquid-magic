########################################################################
# NOTE Currently broken since q processor is busy when we try to peek. #
# This should be fixed when we instead use the in_use flag instead.    #
########################################################################

from functools import partial

import netsquid as ns
from netsquid import sim_reset, sim_time
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ResCreateAndKeep,
    ResMeasureDirectly,
)
from netsquid_magic.magic_distributor import SingleClickMagicDistributor
from netsquid_magic.link_layer import (
    LinkLayerService,
    MagicLinkLayerProtocol,
    SingleClickTranslationUnit,
)


def setup():
    ns.set_qstate_formalism(ns.QFormalism.DM)
    distance = 25  # length needs to be set for single-click MD when there is no heralded connection
    qmemA = QuantumProcessor("qmemA", 1)
    qmemB = QuantumProcessor("qmemB", 2)
    qmemC = QuantumProcessor("qmemC", 1)
    nodeA = Node("nodeA", 0, qmemory=qmemA)
    nodeB = Node("nodeB", 1, qmemory=qmemB)
    nodeC = Node("nodeC", 2, qmemory=qmemC)
    nodes1 = [nodeA, nodeB]
    nodes2 = [nodeB, nodeC]

    magic_distributor1 = SingleClickMagicDistributor(nodes1, length_A=distance, length_B=distance)
    magic_distributor2 = SingleClickMagicDistributor(nodes2, length_A=distance, length_B=distance)

    translation_unit = SingleClickTranslationUnit()

    magic_protocol1 = MagicLinkLayerProtocol(nodes=nodes1, magic_distributor=magic_distributor1, translation_unit=translation_unit)
    magic_protocol2 = MagicLinkLayerProtocol(nodes=nodes2, magic_distributor=magic_distributor2, translation_unit=translation_unit)

    return nodeA, nodeB, nodeC, magic_protocol1, magic_protocol2


class User:
    def __init__(self, nodes):
        self.nodes = nodes
        self.create_ids = []

    def handle_msg(self, node_id, msg):
        print("Node {}: Time {}: Received message {} from link layer service.".format(self.nodes[node_id].name, sim_time(), msg))

        if isinstance(msg, ResCreateAndKeep):
            # Get the state from the memory of one of the nodes
            qstate = self.nodes[node_id].qmemory.peek(msg.logical_qubit_id)[0].qstate
            print("Node {}: Time {}: State of the entangled qubits is: \n{}".format(self.nodes[node_id].name, sim_time(), qstate))
        elif isinstance(msg, ResMeasureDirectly):
            print("Seq {}: Outcome/basis {}/{}".format(msg.sequence_number, msg.measurement_outcome, msg.measurement_basis))


def main():
    sim_reset()

    # Setup nodes and magic protocols for the two links
    nodeA, nodeB, nodeC, magic_protocol1, magic_protocol2 = setup()

    # Setup handlers for OK messages
    nodes = [nodeA, nodeB, nodeC]
    user = User(nodes={node.ID: node for node in nodes})
    handle_reactionA = partial(user.handle_msg, nodeA.ID)
    handle_reactionB = partial(user.handle_msg, nodeB.ID)
    handle_reactionC = partial(user.handle_msg, nodeC.ID)

    # Setup service interfaces for the two links
    service_interfaceA = LinkLayerService(nodeA, magic=True, magic_protocol=magic_protocol1, reaction_handler=handle_reactionA)
    service_interfaceB1 = LinkLayerService(nodeB, magic=True, magic_protocol=magic_protocol1, reaction_handler=handle_reactionB)
    service_interfaceB2 = LinkLayerService(nodeB, magic=True, magic_protocol=magic_protocol2, reaction_handler=handle_reactionB)
    service_interfaceC = LinkLayerService(nodeC, magic=True, magic_protocol=magic_protocol2, reaction_handler=handle_reactionC)

    # Start the service interfaces
    service_interfaceA.start()
    service_interfaceB1.start()
    service_interfaceB2.start()
    service_interfaceC.start()

    # Install rule at B to be able to recv from A
    recv_requestA = ReqReceive(remote_node_id=nodeA.ID)
    recv_requestC = ReqReceive(remote_node_id=nodeC.ID)
    service_interfaceB1.put(recv_requestA)
    service_interfaceB2.put(recv_requestC)

    # NOTE For the linter
    _ = ReqCreateAndKeep
    _ = ReqMeasureDirectly

    # You can change request type here
    create_request = ReqCreateAndKeep(number=1, minimum_fidelity=0.9, remote_node_id=nodeB.ID)

    service_interfaceA.put(create_request)
    service_interfaceC.put(create_request)

    # sim_run()


if __name__ == '__main__':
    main()

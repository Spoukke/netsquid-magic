from collections import namedtuple

from netsquid import sim_reset, sim_run
from netsquid.nodes.node import Node
from netsquid.protocols.serviceprotocol import ServiceProtocol
from netsquid_physlayer.classical_connection import ClassicalConnection

Question = namedtuple("Question", ["question"])
Answer = namedtuple("Answer", ["answer"])


class QuestionAnswerServiceProtocol(ServiceProtocol):

    req_question = Question
    res_answer = Answer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.register_request(self.req_question, self.send_question)
        self.register_response(self.res_answer)


class QuestionAnswerProtocol(QuestionAnswerServiceProtocol):

    questions = ["qubits?"]

    def __init__(self, node, port_name, *args, **kwargs):
        super().__init__(node, *args, **kwargs)
        self._port_name = port_name
        self._setup_ports()

    def _setup_ports(self):
        self.node.ports[self.port_name].bind_input_handler(self.process_data)

    @property
    def port_name(self):
        return self._port_name

    def process_data(self, message):
        msg = message.items[0]

        if msg in self.questions:
            print("Node {} was asked the question {}".format(self.node.name, msg))

            my_nr_qubits = self.node.nr_qubits
            self.send_msg(my_nr_qubits)
        else:
            self.handle_answer(msg)

    def send_question(self, question):
        if question.question not in self.questions:
            raise ValueError("Not a valid question")

        self.send_msg(question.question)

    def handle_answer(self, answer):
        print("Node {} received the answer {}".format(self.node.name, answer))
        response = Answer(answer)
        self.send_response(response)

    def send_msg(self, msg):
        self.node.ports[self.port_name].tx_output(msg)


def setup_base_protocol(protocol_class, magic=False):
    sim_reset()

    nodeA = Node("A", 0, port_names=["classical"])
    nodeA.nr_qubits = 3
    nodeB = Node("B", 1, port_names=["classical"])
    nodeB.nr_qubits = 5
    # if magic:
    #     return nodeA, nodeB, protocol_class(nodes=[nodeA, nodeB])
    # else:
    conn = ClassicalConnection(name="classical", delay=10)
    nodeA.connect_to(nodeB, connection=conn, local_port_name="classical", remote_port_name="classical")

    if magic:
        protoA = protocol_class(nodeA)
        protoB = protocol_class(nodeB)
        protoA.add_other_service(protoB)
        protoB.add_other_service(protoA)
    else:
        protoA = protocol_class(nodeA, port_name="classical")
        protoB = protocol_class(nodeB, port_name="classical")

    return nodeA, nodeB, protoA, protoB


def main():
    nodeA, nodeB, protoA, protoB = setup_base_protocol(QuestionAnswerProtocol)
    protoA.start()
    protoB.start()

    protoA.put(Question("qubits?"))

    sim_run()


if __name__ == '__main__':
    main()

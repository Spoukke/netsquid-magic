from netsquid.nodes.connections import Connection
from netsquid.components.component import Message
from netsquid.qubits.ketstates import BellIndex
from netsquid_physlayer.detectors import BSMOutcome
from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import DepolariseWithFailureStateSamplerFactory


class AbstractHeraldedConnection(Connection):
    """Heralded connection that can hold a few abstract parameters. Made exclusively for use with `AbstractHeraldedMagic`.

    Parameters
    ----------
    name : str
        Name of the component.
    length : float
        Length [km] of the connection.
    fidelity : float (optional)
        Fidelity with which entangled states can be created on this heralded connection. Defaults to 1.
    efficiency : float (optional)
        Success probability per multiplexing mode when there is no attenuation. Defaults to 1.
    num_modes : int (optional)
        Number of modes used to multiplex entanglement generation. Defaults to 1.
    attenuation_coefficient : float (optional)
        Attenuation coefficient [dB/km] of the connection. Defaults to 0.2.
    speed_of_light : float (optional)
        Speed of light [km/s] in the connection. Heralding happens with speed-of-light delay. Defaults to 200,000.

    """
    def __init__(self, name, length, fidelity=1., efficiency=1., num_modes=1, attenuation_coefficient=0.2,
                 speed_of_light=200000., **kwargs):
        super().__init__(name=name)
        self._length = length
        self._fidelity = fidelity
        self._efficiency = efficiency
        self._num_modes = num_modes
        self._attenuation_coefficient = attenuation_coefficient
        self._speed_of_light = speed_of_light

    @property
    def length(self):
        """Length [km] of the connection."""
        return self._length

    @property
    def fidelity(self):
        """Fidelity with which entangled states can be created on this heralded connection."""
        return self._fidelity

    @property
    def efficiency(self):
        """The probability of succeeding at entanglement generation when there is no attenuation.

        That is, the efficiency is the success probability when `length` is zero.
        """
        return self._efficiency

    @property
    def num_modes(self):
        """The number of multiplexing modes used for heralded entanglement generation.

        When multiplexing is used in heralded entanglement generation, this means that effectively multiple attempts
        are performed at the same time. This boosts the probability of obtaining at least a single success in a single
        round. Multiplexing can, e.g., be performed by performing heralded entanglement generation simultaneously
        with photons in different frequency modes. Other types of modes that can be used for multiplexing include
        spatial modes and temporal modes (i.e., different time bins).

        It is assumed here that if there are successes during a single round in multiple modes, still only one entangled
        state is obtained. Additional successes are thus not utilized.

        Let p1 be the success probability per attempt for a single mode.
        Then the probability of all modes failing is (1 - p1) ^ num_modes.
        The probability that entanglement is successfully generated is the probability that there is at least one
        success, which is equal to the probability that not all modes fail.
        Hence, the success probability is given by 1 - (1 - p1) ^ num_modes.
        """
        return self._num_modes

    @property
    def attenuation_coefficient(self):
        """Attenuation coefficient [db/km] of the connection.

        The success probability for a single multiplexing mode accounting only for fiber attenuation is given by
        10 ^ (- attenuation_coefficient * length / 10).
        The total success probability is this success probability multiplied by (1 - efficiency).
        """
        return self._attenuation_coefficient

    @property
    def speed_of_light(self):
        """Speed of light [km/s] of the connection.

        When an attempt is performed, the amount of time it takes before the success or failure of the attempt is
        heralded is determined by the speed-of-light delay.
        More precisely, the time it takes is the connection length divided by the speed of light.
        """
        return self._speed_of_light

    @property
    def prob_max_mixed(self):
        """Depolarizing probability from Werner-state fidelity.

        Produced state is (1 - p) * Bell state + p * maximally mixed state.
        This method returns the value of p.

        The fidelity F of the state to the Bell state is (1 - p) + p / 4 (maximally mixed state = 1 / 4 in 4 dimensions).
        That is, F = 1 - 3 * p / 4.
        Inverting this equation results in p = 4 / 3 * (1 - F).
        """
        return (1 - self.fidelity) * 4 / 3

    @property
    def success_probability(self):
        """Success probability of a single attempt at entanglement distribution."""
        success_probability = 10 ** (- self.attenuation_coefficient / 10 * self.length)  # attenuation loss probability
        success_probability *= self.efficiency  # multiplied by fixed loss probability
        if self.num_modes != 1:
            success_probability = 1 - (1 - success_probability) ** self.num_modes  # accounting for multiplexing
        return success_probability

    @property
    def speed_of_light_delay(self):
        """Time [ns] required for light to travel the length of the channel."""
        return self.length / self.speed_of_light * 1E9


class AbstractHeraldedMagic(MagicDistributor):
    """Magic distributor for a simple, abstracted model of heralded entanglement distribution.

    This magic distributor distributes Werner states between nodes of the form
    rho = (1 - p) phi_plus + p max_mixed_state,
    where rho is the distributed density matrix, p is the `prob_max_mixed` parameter, phi_plus is the Bell state
    phi_plus = |Phi+><Phi+| with |Phi+> = (|00> + |11>) / sqrt(2),
    and max_mixed_state is the maximally mixed state max_mixed_state = I / 4 (where I is the four-dimensional
    unit matrix).

    Entanglement is distributed during discrete attempts. Each attempt is either a success or a failure.
    Whether an attempt is a success or a failure is heralded at the end of the attempt.
    The magic distributor skips all failure rounds and jumps straight to the first success.
    Failure rounds are thus not simulated, but they are accounted for as the time until success is determined
    by sampling from a geometric distribution defined by the success probability `prob_success`.

    This magic distributor was created to be used with :class:`AbstractHeraldedConnection`.
    If such an object is handed as the `component` argument, the magic distributor will configure itself
    to match the physical parameters held by the connection.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
    prob_max_mixed : float or None (optional)
        Probability that a maximally-mixed state is distributed instead of a Bell state.
        If None (which is the default value), `component` cannot be None.
    prob_success : float or None (optional)
        Probability that a single attempt at entanglement distribution is a success.
        If None (which is the default value), `component` cannot be None.
    cycle_time : float or None (optional)
        Duration [ns] of each round of entanglement distribution.
        If None (which is the default value), `component` cannot be None.
    heralded_connection : :obj:`AbstractHeraldedConnection` or None (optional)
        Component from where parameters to configure this magic distributor are obtained.
        Values of `prob_max_mixed`, `prob_success` and `cycle_time` that are passed directly to the constructor
        overwrite values that would have been obtained from the component.

    """
    def __init__(self, nodes, prob_max_mixed=None, prob_success=None, cycle_time=None, heralded_connection=None,
                 **kwargs):
        if heralded_connection is None and (prob_max_mixed is None or prob_success is None or cycle_time is None):
            raise ValueError(
                "If heralded_connection is None, then prob_max_mixed, prob_success and cycle_time cannot be None.")

        # make sure we only pass parameters that are not None (otherwise they overwrite values from the component)
        kwargs.update({"prob_max_mixed": prob_max_mixed, "prob_success": prob_success, "cycle_time": cycle_time,
                       "label_delay": cycle_time})
        kwargs_not_none = {key: value for (key, value) in kwargs.items() if value is not None}

        super().__init__(delivery_sampler_factory=DepolariseWithFailureStateSamplerFactory(),
                         nodes=nodes, state_delay=0., component=heralded_connection, **kwargs_not_none)

    @staticmethod
    def _create_label_message(label):
        """This message type matches what is expected by the protocols used in these simulations."""
        return Message(items=BSMOutcome(success=True, bell_index=BellIndex.PHI_PLUS), id="qdetector")

    def _read_params_from_nodes_or_component(self):
        """Read parameters from `component` specified in the constructor and use it to configure the distributor."""
        [connection] = self._component
        if connection is None:
            return {}
        if not isinstance(connection, AbstractHeraldedConnection):
            raise ValueError(f"Can only read parameters from a {AbstractHeraldedConnection}.")
        return {
            "label_delay": connection.speed_of_light_delay,
            "cycle_time": connection.speed_of_light_delay,
            "prob_max_mixed": connection.prob_max_mixed,
            "prob_success": connection.success_probability,
        }

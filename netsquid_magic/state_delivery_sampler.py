from abc import ABCMeta
from collections import namedtuple
import numpy as np
import netsquid as ns
from netsquid.qubits.state_sampler import StateSampler
from netsquid.qubits.ketstates import BellIndex, bell_states
import netsquid.qubits.qubitapi as qapi


DeliverySample = namedtuple("DeliverySample", ["state", "delivery_duration", "label"])


class StateDeliverySampler(StateSampler):
    """
    Class for sampling both a quantum state and the time its generation took.
    """

    def __init__(self, state_sampler, cycle_time):
        """
        Parameters
        ----------
        state_sampler : :obj:`~netsquid_magic.state_sampler.StateSampler`
        cycle_time : float or int
            Time that a single attempt at generating the state takes.
        """
        self._assert_positive_time(cycle_time, param_name="cycle_time")
        self._cycle_time = float(cycle_time)

        if not isinstance(state_sampler, StateSampler):
            raise TypeError("Parameter `state_sampler` should be a StateSampler")
        self._state_sampler = state_sampler

        self._success_probability = 1. - self._state_sampler.probability_of_none

    @staticmethod
    def _assert_positive_time(time, param_name=""):
        """Asserts that ``time`` is a positive time."""
        if not (isinstance(time, float) or isinstance(time, int)):
            raise TypeError("Parameter {} should be a float or int".format(param_name))
        if time < 0.:
            raise ValueError("Parameter {} should be a positive number".format(param_name))

    def sample(self, skip_rounds=True):
        """
        Samples a quantum state and the time its generation took, together with
        a classical flag. In the case that the (entangled) quantum state was
        produced between two remote nodes using photon detectors in a midpoint,
        this classical flag corresponds to which of the two detectors in the
        midpoint clicked, which provides information about the produced state.

        Parameters
        ----------
        skip_rounds : bool
            If True, then only sample from states that are not None and skip rounds which failed.
            If False, also sample from None and schedule fails.

        Returns
        -------
        :obj:`~netsquid_magic.state_delivery_sampler.DeliverySample` object, which
        contains:
              * the sampled quantum state
              * total (simulated) time that generating (delivering) this quantum state
                would have taken if the entire generation process had been simulated
              * outcome from the midpoint, e.g. which of the two detectors clicked.

        Notes
        -----
        The delivery duration is sampled from a geometric distribution with parameter
        :math:`1-p_{None}`, where :math:`p_{None}` is the total probability of the leaves
        in the :obj:`~netsquid_magic.state_sampler.StateSampler` that are `None`.
        """
        successful_state, probability, label = self._state_sampler.sample(exclude_none=skip_rounds)
        # The geometric samples positive integers so we subtract one to allow for success at the start in the first round
        number_of_attempts = np.random.geometric(p=self._success_probability) - 1
        generation_duration = \
            self._cycle_time * number_of_attempts
        return DeliverySample(successful_state, generation_duration, label)
        # TODO extend? where should the generated noisemodel go?


class IStateDeliverySamplerFactory(metaclass=ABCMeta):
    """
    Abstract base class for a factory that produces
    :obj:`~netsquid_magic state_delivery_factory.StateDeliverySampler`
    objects.
    """

    def create_state_delivery_sampler(self, **kwargs):
        pass


class HeraldedStateDeliverySamplerFactory(IStateDeliverySamplerFactory, metaclass=ABCMeta):
    r"""
    Abstract base class for a factory that produces
    :obj:`~netsquid_magic state_delivery_factory.StateDeliverySampler`
    objects for which the underlying tree is of the form

    .. code-block:: text

            +-------+
            |  root |
            +-------+
            //      \\
           //        \\
          //          \\
         //            \\
        None            \\
                         \\
                 +------------------+
                 |      success     |
                 +------------------+
                //   ||    \\       \\
               //    ||     \\       \\
              //     ||      \\       \\
             State0  State1  State2  ....

    Example usage for delivering:

      * the mixed state :math:`0.25 |0\rangle\langle 0| + 0.75 |1\rangle\langle1|`
      * which is generated in attempts, each of which succeeds with probability 0.3
      * the cycle time (time of a single attempt) is 10

    .. code-block:: python

        from netsquid.qubits.ketstates import s0, s1, ket2dm
        from netsquid.qubits.state_sampler import StateSampler
        from netsquid_magic.state_sampler.state_delivery_sampler import HeraldedStateDeliverySamplerFactory

        # Case A: the mixed state is in ket form and the success probability is 30%

        def func_delivery_ket(p, **kwargs):
            kets = [s0, s1]
            probabilities = [p, 1 - p]
            return StateSampler(qreprs=kets, probabilities=probabilities), 0.3

        # Case B: the mixed state is in density matrix form and the success probability is 30%

        def func_delivery_dm(p, **kwargs):
            dm = p * ket2dm(s0) + (1 - p) * ket2dm(s1)
            return StateSampler(qreprs=[dm], probabilities=[1.]), 0.3

        # Creating a DeliverySamplerFactory with either of the two functions

        for func_delivery in [func_delivery_ket, func_delivery_dm]:
            sampler_factory = HeraldedStateDeliverySamplerFactory(func_delivery)

            parameters = {'p': 0.25, 'cycle_time': 10}
            print(sampler_factory.create_state_delivery_sampler(**parameters).sample())
        # DeliverySample(state=array([[0.+0.j],
        #     [1.+0.j]]), delivery_duration=10.0, label=('success', 1))
        # DeliverySample(state=array([[0.25+0.j, 0.  +0.j],
        #     [0.  +0.j, 0.75+0.j]]), delivery_duration=50.0, label=('success', 0))
    """

    LABELS = ["success", "fail"]

    def __init__(self, func_delivery):
        """
        Parameters
        ----------
        func_delivery : function which outputs a tuple `(state_sampler, success_probability)`
                        where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
                        object and `success_probability` is a float.
        """
        self._func_delivery = func_delivery

    def create_state_delivery_sampler(self, cycle_time, **parameters):
        """
        Parameters
        ----------
        parameters : dictionary with str as keys and Any as values
            Parameters that will be passed to the delivery function :func:`_func_delivery`.

        Returns
        -------
        :obj:`~netsquid_magic.state_delivery_factory.StateDeliverySampler`
            The labels on the edges at the top level are 'success' and 'fail' with respective probabilities of
            success_probability and (1 - success_probability) as specified by the output of :func:`_func_delivery`.
        """
        successful_state_sampler, success_probability = self._func_delivery(**parameters)

        if not .0 <= success_probability <= 1.:
            raise ValueError(f"success_probability {success_probability} not a valid probability.")

        full_state_sampler = \
            StateSampler(qreprs=[successful_state_sampler, None],
                         probabilities=[success_probability, 1. - success_probability],
                         labels=self.LABELS)
        return StateDeliverySampler(state_sampler=full_state_sampler,
                                    cycle_time=cycle_time)


class PerfectStateSamplerFactory(HeraldedStateDeliverySamplerFactory):
    def __init__(self):
        super().__init__(func_delivery=self._get_perfect_state_sampler)

    @staticmethod
    def _get_perfect_state_sampler(**kwargs):
        dm_epr_state = np.array(
            [[0.5, 0, 0, 0.5],
             [0, 0, 0, 0],
             [0, 0, 0, 0],
             [0.5, 0, 0, 0.5]],
            dtype=np.complex)

        return StateSampler(qreprs=[dm_epr_state], probabilities=[1], labels=[BellIndex.B00]), 1


class DepolariseStateSamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    A factory for samplers that produce either an EPR pair, or the maximally
    mixed state over the two 2 nodes (I/4).
    """
    def __init__(self):
        super().__init__(func_delivery=self._delivery_func)

    @staticmethod
    def _delivery_func(prob_max_mixed, **kwargs):
        """
        Parameters
        ----------
        prob_max_mixed : float
            Probability that instead of perfect EPR pair,
            the maximally mixed state is distributed.

        Returns
        -------
        tuple `(state_sampler, success_probability)`
            where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
            object and `success_probability` is a float.
        """
        epr_state = np.array(
            [[0.5, 0, 0, 0.5],
             [0, 0, 0, 0],
             [0, 0, 0, 0],
             [0.5, 0, 0, 0.5]],
            dtype=np.complex)
        maximally_mixed = np.array(
            [[0.25, 0, 0, 0],
             [0, 0.25, 0, 0],
             [0, 0, 0.25, 0],
             [0, 0, 0, 0.25]],
            dtype=np.complex)
        return StateSampler(qreprs=[epr_state, maximally_mixed], probabilities=[1 - prob_max_mixed, prob_max_mixed],
                            labels=[BellIndex.B00, BellIndex.B00]), 1


class DepolariseWithFailureStateSamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    A factory for samplers that with a given success probability produce a mixture of a perfect EPR pair and a maximally
    mixed state over the two nodes.
    """
    def __init__(self):
        super().__init__(func_delivery=self._delivery_func)

    @staticmethod
    def _delivery_func(prob_max_mixed, prob_success, **kwargs):
        """
        Parameters
        ----------
        prob_max_mixed : float
            Fraction of maximally mixed state in the mixture.
        prob_success : float
            Probability of successfully sampling.

        Returns
        -------
        tuple `(state_sampler, success_probability)`
            where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
            object and `success_probability` is a float.
        """
        epr_state = np.array(
            [[0.5, 0, 0, 0.5],
             [0, 0, 0, 0],
             [0, 0, 0, 0],
             [0.5, 0, 0, 0.5]],
            dtype=np.complex)
        maximally_mixed = np.array(
            [[0.25, 0, 0, 0],
             [0, 0.25, 0, 0],
             [0, 0, 0.25, 0],
             [0, 0, 0, 0.25]],
            dtype=np.complex)
        state_to_deliver = (1 - prob_max_mixed) * epr_state + prob_max_mixed * maximally_mixed
        return StateSampler(qreprs=[state_to_deliver], probabilities=[1], labels=[BellIndex.B00]), prob_success


class BitflipStateSamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    A factory for samplers that produce either a perfect EPR pair,
    or an EPR pair where an X gate is applied to one of the qubits ("bit flip").
    """
    def __init__(self):
        super().__init__(func_delivery=self._delivery_bit_flip)

    @staticmethod
    def _delivery_bit_flip(flip_prob, **kwargs):
        """
        Parameters
        ----------
        flip_prob : float
            Probability that instead of a perfectly correlated EPR pair,
            a perfectly anti-correlated pair is distributed.

        Returns
        -------
        tuple `(state_sampler, success_probability)`
            where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
            object and `success_probability` is a float.
        """
        return StateSampler(qreprs=[ns.b00, ns.b01], probabilities=[1 - flip_prob, flip_prob],
                            labels=[BellIndex.B00, BellIndex.B00]), 1


class SingleClickDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    Delivery sampler factory for single-click protocol. Used in SingleClickMagicDistributor.

    This is an analytical model for the states generated through a single-click entanglement generation protocol. In
    such a protocol, entanglement generation is heralded when a single photon is detected at a midpoint station. This
    model accounts for asymmetry, i.e. it allows for different detection probabilities and bright state populations for
    the two nodes. It also takes into account the effect of several imperfections, such as dark counts, photon
    distinguishability and imperfect detectors.

    The state model is based on three different Hanson group papers:
    "Entanglement Distillation between Solid-State Quantum Network Nodes" (arXiv:1703.03244, section VII of the
    supplementary material), "Deterministic delivery of remote entanglement on a quantum network" (arXiv:1712.07567,
    section III of the supplementary material) and
    "Realization of a multi-node quantum network of remote solid-state qubits" arXiv:2102.04471 (section II of the
    supplementary material). The models in each of these papers are similar, differing only in the imperfections
    considered and in the degree of asymmetry for which they allow. The model here simply combines these models.

    The only exception regards number-resolving detectors: the models in all of the papers mentioned above assume that
    number-resolving detectors are used. In this model, considering detectors that are not number-resolving is also
    possible. Details regarding the model with non number-resolving detectors can be found in UNPUBLISHED_PAPER.

    In the meantime, short explanations of what is done at different steps can be found in the code in the form of
    inline comments.

    TODO Update when Delft-Eindhoven paper is out.

    """
    def __init__(self):
        super().__init__(func_delivery=self._get_single_click_state_sampler)

    @classmethod
    def _get_single_click_state_sampler(cls, alpha_A, alpha_B, length_A, length_B, p_loss_length_A, p_loss_length_B,
                                        p_loss_init_A, p_loss_init_B, detector_efficiency, visibility=1,
                                        dark_count_probability=0, p_fail_class_corr=0, coherent_phase=0,
                                        num_resolving=False, **kwargs):
        r"""Calculate states and their relative probability for successful single-click entanglement generation.

        Parameters
        ----------
        alpha_A : float
            Bright state population of node on side "A" of heralded connection.
        alpha_B : float
            Bright state population of node on side "B" of heralded connection.
        length_A : float
            Fiber length [km] of "A" side of heralded connection.
        length_B : float
            Fiber length [km] of "B" side of heralded connection.
        p_loss_length_A: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_length_B: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_init_A: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        p_loss_init_B: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        detector_efficiency : float
            Probability of detecting a photon given that it impinges on the detector.
        visibility: float
            Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability).
        dark_count_probability : float
            Probability of detecting a photon when there is none.
        coherent_phase : float
            In the absence of all other noise, this sampler produces the state
            :math:`|01\rangle + e^{i\cdot \text{coherent_phase}} |10\rangle`.
        num_resolving: bool
            True if photon-number-resolving detectors are used.

        Returns
        -------
        tuple `(state_sampler, success_probability)`
            where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
            object and `success_probability` is a float.
        """

        p_det_A, p_det_B = cls._compute_total_detection_probability(
            length_A=length_A, length_B=length_B, p_loss_length_A=p_loss_length_A, p_loss_length_B=p_loss_length_B,
            p_loss_init_A=p_loss_init_A, p_loss_init_B=p_loss_init_B, detector_efficiency=detector_efficiency
        )

        dm_states = []

        [p_up_up, p_up_down, p_down_up, p_down_down] = \
            cls._compute_detection_probabilities(
                alpha_A=alpha_A,
                alpha_B=alpha_B,
                p_dc=dark_count_probability,
                p_det_A=p_det_A,
                p_det_B=p_det_B,
                visibility=visibility,
                num_resolving=num_resolving)

        for detector_outcome in [1, -1]:

            # Define the unnormalized matrix of Psi^{\pm}
            s = np.sqrt(visibility * p_up_down * p_down_up) * detector_outcome
            dm_psi = np.array(
                [[0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_up_up, 0., 0., 0.],
                 [0., (1 - p_fail_class_corr) * p_up_down, (1 - p_fail_class_corr) * np.exp(coherent_phase * 1j) * s, 0.],
                 [0., (1 - p_fail_class_corr) * np.exp(-1 * coherent_phase * 1j) * s, (1 - p_fail_class_corr) * p_down_up, 0.],
                 [0., 0., 0., 0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_down_down]],
                dtype=np.complex)

            # normalization
            dm = dm_psi / np.trace(dm_psi)

            dm_states.append(dm)

        # success_probability
        success_probability = sum([p_up_up, p_up_down, p_down_up, p_down_down])

        return StateSampler(qreprs=dm_states, probabilities=[0.5, 0.5],
                            labels=[BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]), success_probability

    @staticmethod
    def _compute_total_detection_probability(length_A, length_B, p_loss_length_A, p_loss_length_B, p_loss_init_A,
                                             p_loss_init_B, detector_efficiency):
        """Calculate the probability that a photon escapes the quantum processor, survives the fibre, and is detected.

        Parameters
        ----------
        length_A : float
            Fiber length [km] of "A" side of heralded connection.
        length_B : float
            Fiber length [km] of "B" side of heralded connection.
        p_loss_length_A: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_length_B: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_init_A: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        p_loss_init_B: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        detector_efficiency : float
            Probability of detecting a photon given that it impinges on the detector.

        Returns
        -------
        photon_prob_A : float
            Probability of detecting a photon given that it was emitted by node on "A" side.
        photon_prob_B : float
            Probability of detecting a photon given that it was emitted by node on "B" side.
        """
        photon_prob_A = (1 - p_loss_init_A) * np.power(10, - p_loss_length_A * length_A / 10) * detector_efficiency
        photon_prob_B = (1 - p_loss_init_B) * np.power(10, - p_loss_length_B * length_B / 10) * detector_efficiency

        return photon_prob_A, photon_prob_B

    @staticmethod
    def _compute_detection_probabilities(alpha_A, alpha_B, p_dc, p_det_A, p_det_B, visibility, num_resolving):
        """Calculate true success probability and false-positive probabilities for single-click entanglement generation.

        This function implements an analytical model for single-click heralded entanglement generation,
        which enumerates all the different events that can result in a single detector click and consequently in
        entanglement generation being heralded. If there are dark counts or non-unit visibility, false positives are
        also possible.

        Parameters
        ----------
        alpha_A : float
            Bright state population of node on side "A" of heralded connection.
        alpha_B : float
            Bright state population of node on side "B" of heralded connection.
        p_dc : float
            Probability of detecting a photon when there is none.
        p_det_A : float
            Probability of detecting a photon given that it was emitted by node on "A" side.
        p_det_B : float
            Probability of detecting a photon given that it was emitted by node on "B" side.
        visibility : float
            Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability).
        num_resolving: bool
            True if photon-number-resolving detectors are used.

        Returns
        -------
        p_up_up : float
            Probability of success being heralded if both spins are in the bright state.
        p_up_down : float
            Probability of success being heralded if spin A is in the bright state but spin B is not.
        p_down_up : float
            Probability of success being heralded if spin B is in the bright state but spin A is not.
        p_down_down : float
            Probability of success being heralded if no spins are in the bright state.
        """

        # The probability that the two photons go to the same detector
        # (taken from section VIII of the supplementary material of arXiv:1703.03244)
        prob_different_detectors = (1 - visibility) / 2
        prob_same_detectors = 1 - prob_different_detectors

        # We split the analysis into four situations, following the convention established in the
        # aforementioned papers. These correspond to the probability of event being heralded as success if:
        # 1. Both spins are in the bright state
        # 2. Spin A is in the bright state, spin B is not
        # 3. Spin A is not in the bright state, spin B is
        # 4. Neither of the spins is in the bright state

        # Case 1: both spins emit a photon, in which case we get success if:
        # (a) Only one of the emitted photons survives. For number-resolving detectors, there cannot be dark counts in
        # either of the detectors (as otherwise two photons would be detected in the detector which also saw the actual
        # emitted photon, and the event would be heralded as a failure). For non-number-resolving detectors, the
        # requirement is just that there is no dark count in the detector that did not detect the emitted photon.
        if num_resolving:
            case_up_up_a = (1 - p_dc) ** 2 * (p_det_A * (1 - p_det_B) + p_det_B * (1 - p_det_A))
        else:
            case_up_up_a = (1 - p_dc) * (p_det_A * (1 - p_det_B) + p_det_B * (1 - p_det_A))

        # (b) No emitted photon is detected, and there is a dark count in one of the detectors. This case is the same
        # irrespective of whether the detectors are number resolving. There is a factor of two because this can happen
        # in either detector
        case_up_up_b = 2 * (1 - p_det_A) * (1 - p_det_B) * (1 - p_dc) * p_dc

        # (c) Both emitted photons make it to the midpoint and are detected, but they bunch and go into the same
        # detector. Furthermore, there is no dark count in the other detector. There is a factor of two because this can
        # happen in either detector. Note that this is heralded as a failure if the detectors are number resolving.
        case_up_up_c = p_det_A * p_det_B * prob_same_detectors * (1 - p_dc)

        if num_resolving:
            p_up_up = alpha_A * alpha_B * (case_up_up_a + case_up_up_b)
        else:
            p_up_up = alpha_A * alpha_B * (case_up_up_a + case_up_up_b + case_up_up_c)

        # Case 2: Only spin A emits a photon, in which case we get success if:
        # (a) The emitted photon survives. For number-resolving detectors, there cannot be dark counts in
        # either of the detectors (as otherwise two photons would be detected in the detector which also saw the actual
        # emitted photon, and the event would be heralded as a failure). For non-number-resolving detectors, the
        # requirement is just that there is no dark count in the detector that did not detect the emitted photon.
        if num_resolving:
            case_up_down_a = (1 - p_dc) ** 2 * p_det_A
        else:
            case_up_down_a = (1 - p_dc) * p_det_A

        # (b) The emitted photon does not survive, and there is a dark count in one of the detectors. This case is the
        # same irrespective of whether the detectors are number resolving. There is a factor of two because this can
        # happen in either detector.
        case_up_down_b = 2 * (1 - p_det_A) * (1 - p_dc) * p_dc

        p_up_down = alpha_A * (1 - alpha_B) * (case_up_down_a + case_up_down_b)

        # Case 3: Only spin B emits a photon. This is identical to case 2, substituting A for B.

        if num_resolving:
            case_down_up_a = (1 - p_dc) ** 2 * p_det_B
        else:
            case_down_up_a = (1 - p_dc) * p_det_B

        case_down_up_b = 2 * (1 - p_det_B) * (1 - p_dc) * p_dc

        p_down_up = alpha_B * (1 - alpha_A) * (case_down_up_a + case_down_up_b)

        # Case 4: No photon is emitted. In this case, we only get success if there is a dark count in one of the
        # detectors, but not the other. This case is the same irrespective of whether the detectors are number
        # resolving. There is a factor of two because this can happen in either detector.

        p_down_down = 2 * (1 - alpha_A) * (1 - alpha_B) * p_dc * (1 - p_dc)

        return [p_up_up, p_up_down, p_down_up, p_down_down]


class DoubleClickDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):
    """Delivery sampler factory for double-click protocol. Used in DoubleClickMagicDistributor.

    See nlblueprint-docs repo for detailed explanation of the model used here.
    Short explanation of what is done at different steps can be found in the code
    in the form of inline comments.

    """

    def __init__(self):
        super().__init__(func_delivery=self._func_delivery)
        self._pt = None
        self._pf1 = None
        self._pf2 = None
        self._pf3 = None
        self._pf4 = None
        self._parameters = None
        self.success_probability = None
        self.state_sampler = None

    def _func_delivery(self, length_A, length_B, p_loss_length_A, p_loss_length_B,
                       p_loss_init_A, p_loss_init_B, emission_fidelity_A, emission_fidelity_B,
                       detector_efficiency, dark_count_probability, visibility, num_resolving, num_multiplexing_modes,
                       coin_prob_ph_ph, coin_prob_ph_dc, coin_prob_dc_dc, **kwargs):
        """Calculate states and their relative probability for successful double-click entanglement generation.

        Parameters
        ----------
        length_A : float
            Fiber length [km] of "A" side of heralded connection.
        length_B : float
            Fiber length [km] of "B" side of heralded connection.
        p_loss_length_A: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_length_B: float
            Attenuation coefficient [db/km] of fiber on "A" side of heralded connection.
        p_loss_init_A: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        p_loss_init_B: float
            Probability that a photon gets lost when entering heralded connection on "A" side.
        detector_efficiency : float
            Probability of detecting a photon given that it impinges on the detector.
        dark_count_probability : float
            Probability of detecting a photon when there is none.
        visibility: float
            Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability).
        num_resolving: bool
            True if photon-number-resolving detectors are used.
        num_multiplexing_modes : int
            Number of modes used for multiplexing, i.e. how often entanglement generation is attempted per round.
        emission_fidelity_A : float
            Fidelity of state shared between photon and memory qubit on "A" side to
            :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission.
        emission_fidelity_B : float
            Fidelity of state shared between photon and memory qubit on "B" side to
            :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission.
        coin_prob_ph_ph : float
            Coincidence probability for two photons. When using a coincidence time window in the double-click protocol,
            two clicks are only accepted if they occurred within one coincidence time window away from each other.
            This parameter is the probability that if both clicks are photon detections,
            they are within one coincidence window. In general, this depends not only on the size of the coincidence
            time window, but also on the state of emitted photons and the total detection time window.
        coin_prob_ph_dc : float
            Coincidence probability for a photon and a dark count.
            When using a coincidence time window in the double-click protocol,
            two clicks are only accepted if they occurred within one coincidence time window away from each other.
            This parameter is the probability that if one click is a photon detection and the other a dark count,
            they are within one coincidence window. In general, this depends not only on the size of the coincidence
            time window, but also on the state of emitted photons and the total detection time window.
        coin_prob_dc_dc : float
            Coincidence probability for two dark counts. When using a coincidence time window in the double-click protocol,
            two clicks are only accepted if they occurred within one coincidence time window away from each other.
            This parameter is the probability that if both clicks are dark counts,
            they are within one coincidence window. In general, this depends on the size of the coincidence time window
            and the total detection time window.

        Returns
        -------
        tuple `(state_sampler, success_probability)`
            where `state_sampler` is a :obj:`~netsquid.qubits.state_sampler.StateSampler`
            object and `success_probability` is a float.
        """
        new_parameters = {
            "length_A": length_A,
            "length_B": length_B,
            "p_loss_length_A": p_loss_length_A,
            "p_loss_length_B": p_loss_length_B,
            "p_loss_init_A": p_loss_init_A,
            "p_loss_init_B": p_loss_init_B,
            "detector_efficiency": detector_efficiency,
            "dark_count_probability": dark_count_probability,
            "visibility": visibility,
            "num_resolving": num_resolving,
            "num_multiplexing_modes": num_multiplexing_modes,
            "emission_fidelity_A": emission_fidelity_A,
            "emission_fidelity_B": emission_fidelity_B,
            "coin_prob_ph_ph": coin_prob_ph_ph,
            "coin_prob_ph_dc": coin_prob_ph_dc,
            "coin_prob_dc_dc": coin_prob_dc_dc
        }
        # only calculate the model if it hasn't been calculated already for same parameters during the last call
        # if parameters are the same as during the last call, the result of that call is reused
        if self._parameters != new_parameters:

            # set new parameters
            self._parameters = new_parameters

            # calculate different detection probabilities for the new parameters
            self._calculate_probabilities()

            # calculate success probability with the new parameters
            self.success_probability = self._calculate_success_probability()

            # calculate the states to be delivered with the new parameters
            labels = [BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]
            states = [self._calculate_density_matrix(bell_index=bell_index)
                      for bell_index in labels]
            self.state_sampler = StateSampler(qreprs=states, probabilities=[.5, .5], labels=labels)

        return self.state_sampler, self.success_probability

    def _calculate_probabilities(self):
        """ Calculate true success probability and false-positive probabilities for dual-rail elementary link.

        This function implements an analytical model for dual-rail heralded entanglement generation,
        which enumerates all the different events that can result in a click pattern that is associated
        with successful entanglement generation. If there are dark counts or non-unit visibility, false positives are also possible.

        Returns
        -------
        real_success_prob : float
            Probability of a heralded success that is not caused by dark counts and results from photons behaving
            indistinguishably (i.e. they interfere).
            This detection event results in a maximally entangled state at the photon emitters in the absence
            of other types of noise.
        false_success_prob_1 : float
            Probability of a heralded success that is not caused by dark counts but results from photons behaving
            distinguishably (i.e. they do not interfere).
            This detection event results in a classically anticorrelated state at the photon emitters in the absence
            of other types of noise.
        false_success_prob_2 : float
            Probability of a heralded success caused by two photons going to the same detector and a dark count.
            This detection event results in a classically correlated state at the photon emitters in the absence
            of other types of noise.
        false_success_prob_3 : float
            Probability of a heralded success caused by one photon getting lost and a dark count.
            This detection event bears no information about correlation between photons and thus results in a
            maximally mixed state at the photon emitters.
        false_success_prob_4 : float
            Probability of a heralded success caused by both photons getting lost and two dark counts.
            This detection event bears no information about correlation between photons and thus results in a
            maximally mixed state at the photon emitters.

        Note
        ----
        Details of the calculation can be found in the nlblueprint docs.

        """
        if self._parameters is None:
            raise RuntimeError("No parameters have been set yet.")

        # Probability that photon emitted by node A arrives at the midpoint detector:
        photon_prob_A = ((1 - self._parameters["p_loss_init_A"]) *
                         np.power(10, - self._parameters["p_loss_length_A"] * self._parameters["length_A"] / 10) *
                         self._parameters["detector_efficiency"])
        # Probability that photon emitted by node B arrives at the midoint detector:
        photon_prob_B = ((1 - self._parameters["p_loss_init_B"]) *
                         np.power(10, - self._parameters["p_loss_length_B"] * self._parameters["length_B"] / 10) *
                         self._parameters["detector_efficiency"])

        visibility = self._parameters["visibility"]
        num_resolving = self._parameters["num_resolving"]
        dark_count_probability = self._parameters["dark_count_probability"]
        coin_prob_ph_ph = self._parameters["coin_prob_ph_ph"]
        coin_prob_ph_dc = self._parameters["coin_prob_ph_dc"]
        coin_prob_dc_dc = self._parameters["coin_prob_dc_dc"]

        # Note: we include detector_efficiency in the arrival probability of the photons.
        # The reason for this, is that when a photon arrives but is not detected,
        # this has the same effect as when it had been lost before reaching the midpoint.

        # An entangled state is created if both photons are detected at the midpoint,
        # in different modes (dual-rail encoding is defined by using two different modes,
        # e.g. polarization or time bin), and they behave as if indistinguishable.
        # Being detected in different modes gives an overall factor .5.
        # The probability that they behave as if indistinguishable happens exactly with probability equal
        # to the visibility.
        # Furthermore, there must be no dark counts in the two detectors at which no photons arrive.
        # Finally, if a coincidence time window is used, the click pattern is only accepted in case
        # the two photons are detected within the same coincidence time window
        # (i.e. only if the time between the two clicks is not too large).
        # The probability of this happening is:
        real_success_prob = .5 * photon_prob_A * photon_prob_B * coin_prob_ph_ph * visibility
        if num_resolving:
            real_success_prob *= (1 - dark_count_probability) ** 4
        else:
            # if not number resolving, dark counts at the detectors that already click have no effect
            real_success_prob *= (1 - dark_count_probability) ** 2

        # if two photons arrive and are detected in different modes, but don't behave as if indistinguishable,
        # there is no interference and a non-entangled (but still anticorrelated state) is created.
        # This has the same probability as the true success probability above,
        # but with visibility replaced by 1 - visibility.
        false_success_prob_1 = .5 * photon_prob_A * photon_prob_B * coin_prob_ph_ph * (1 - visibility)
        if num_resolving:
            false_success_prob_1 *= (1 - dark_count_probability) ** 4
        else:
            # if not number resolving, dark counts at the detectors that already click have no effect
            false_success_prob_1 *= (1 - dark_count_probability) ** 2

        # If both photons arrive in the same polarization / time bin,
        # they can end up at the same detector.
        # They end up at the same detector with probability 1 if they are indistinguishable, and with probability
        # 0.5 if they are not.
        # This gives a probability V + (1 - V) / 2 = (1 + V) / 2 (with V = visibility).
        # For non-photon-number-resolving detectors,
        # this will give a false positive if simultaneously, there is a dark count in a detector corresponding
        # to the mode that the photons are not in.
        # There's two detectors that qualify, giving a factor 2 that cancels against the factor 0.5 for the photons
        # being in the same mode.
        # If a coincidence time window is used,
        # we multiply by the coincidence probability for a photon and a dark count
        # (i.e. the probability that a single photon and a dark count are detected within the same coincidence window).
        # This may not be entirely correct however, as there are two photons in play instead of one.
        # For photon-number-resolving detectors, two photons in the same detector leads to a heralded failure,
        # thus the probability of this case occurring is zero.
        # This leads to:
        false_success_prob_2 = 0 if num_resolving else (.5 * photon_prob_A * photon_prob_B * (1 + visibility) *
                                                        dark_count_probability * coin_prob_ph_dc *
                                                        (1 - dark_count_probability) ** 2)

        # Another false positive is given in the case that only one photon is detected at the midpoint,
        # if there is a dark count in a detector not corresponding to the mode of the detector that clicked.
        # Just as in the previous term, there is a factor 2 because there are two such detector.
        # The probability of this happening is:
        false_success_prob_3 = ((photon_prob_A * (1 - photon_prob_B) + photon_prob_B * (1 - photon_prob_A)) *
                                coin_prob_ph_dc * 2 * dark_count_probability)
        if num_resolving:
            false_success_prob_3 *= (1 - dark_count_probability) ** 3
        else:
            # if not number resolving, a dark count at the detector that already clicks has no effect
            false_success_prob_3 *= (1 - dark_count_probability) ** 2

        # Finally, a false positive can arise when none of the photons are detected, but there are two dark counts
        # in detectors corresponding to different modes.
        # There are 6 different ways in which you can pick two detectors from the four,
        # and 2 / 3 of all such configurations will correspond to different modes
        # (if the first detector corresponds to mode a, there are three detectors left, two of which correspond to
        # mode b, and one to mode a).
        # Thus, there are 4 configurations with two dark counts in total that lead to a false positive.
        # The probability of this happening is:
        false_success_prob_4 = (4 * (1 - photon_prob_A) * (1 - photon_prob_B) * dark_count_probability ** 2 *
                                coin_prob_dc_dc * (1 - dark_count_probability) ** 2)

        self._pt = real_success_prob
        self._pf1 = false_success_prob_1
        self._pf2 = false_success_prob_2
        self._pf3 = false_success_prob_3
        self._pf4 = false_success_prob_4

    def _calculate_success_probability(self):
        """Calculate the probability that a success is heralded by the double-click protocol."""
        # for a single mode, it is just the sum of the individual "success" detection event probabilities
        success_prob_single_mode = self._pt + self._pf1 + self._pf2 + self._pf3 + self._pf4
        success_prob_all_modes_fail = (1 - success_prob_single_mode) ** self._parameters["num_multiplexing_modes"]
        return 1 - success_prob_all_modes_fail

    def _calculate_density_matrix(self, bell_index):
        """Density matrix expected to be delivered between end nodes by the setup, based on analytical model.

        Parameters
        ----------
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex
            Pauli correction index corresponding to click pattern at heralding station.
            Must be `BellIndex.PSI_PLUS` or `BellIndex.PSI_MINUS`.

        Returns
        -------
        numpy.array
            Expected density matrix.

        Notes
        -----
        It is assumed the emitted photon and memory are in a Werner state.

        """

        if [self._pt, self._pf1, self._pf2, self._pf3, self._pf4] == [0, 0, 0, 0, 0]:
            self._parameters = None  # makes sure this error is thrown again if the same parameters are used again
            raise ValueError("For these parameter values, the state cannot be calculated."
                             "This probably means the success-probability is zero.")

        # Translate emission fidelities to depolarizing parameters, which can be done since we assume the
        # photon - memory entangled state created upon photon emission is a Werner state.
        # Parametrizing the noise in terms of depolarization is useful,
        # since depolarizing noise can be "moved around" within a Bell state,
        # and depolarizing channels are easy to concatenate (D(p1)[D(p2)[rho]] = D(p1 * p2)[rho]).
        # Werner state: p rho_Bell + (1 - p) / 4 * I  (p = depolarizing parameter)
        # Fidelity: F = p + (1 - p) / 4 = 1 / 4 + 3 / 4 * p
        # -> p = (4 F - 1) / 3
        dep_prob_a = (4 * self._parameters["emission_fidelity_A"] - 1) / 3
        dep_prob_b = (4 * self._parameters["emission_fidelity_B"] - 1) / 3
        total_dep_prob = dep_prob_a * dep_prob_b  # called q_em in the notes in nlblueprint-docs

        # We construct a non-normalized density matrix by summing over the different cases
        # that are labelled a success (see :meth:`_calculate_probabilities`),
        # i.e. rho = p1 rho1 + p2 rho2 + p3 rho3 + p4 rho4,
        # where pi is the probability of case i occuring, and rhoi is the quantum state that is created in that case.

        # If the outcome is 1, the "correct" DM (i.e. corresponding to a successful detection that is not a false
        # positive) is Psi+, if it is 2, it is Psi-
        if bell_index is BellIndex.PSI_PLUS:
            psiplus = np.outer(ns.qubits.ketstates.b01, ns.qubits.ketstates.b01)
            dm = self._pt * total_dep_prob * psiplus
        elif bell_index is BellIndex.PSI_MINUS:
            psimin = np.outer(ns.qubits.ketstates.b11, ns.qubits.ketstates.b11)
            dm = self._pt * total_dep_prob * psimin
        else:
            raise ValueError("Label {} given, while only psi+ (Bell index 1) and psi- (Bell index 2) are expected for "
                             "dual-rail heralded entanglement generation.".format(bell_index))

        maxmixed = np.eye(4) / 4  # Maximally mixed state, created as result of depolarizing noise
        classically_correlated = np.diag([1, 0, 0, 1]) / 2  # (|00><00| + |11><11|) / 2
        classically_anticorrelated = np.diag([0, 1, 1, 0]) / 2  # (|01><01| + |10><10|) / 2

        # If two photons are detected in different mode but they didn't behave like indistinguishable photons
        # (i.e. they didn't interfere), the emitters will be anticorrelated, but only classically so.
        dm += self._pf1 * total_dep_prob * classically_anticorrelated

        # If two photons are detected at the same detector, they are in the same mode, but we don't know which.
        # Note: if a qubit gets depolarized, the final state is maximally mixed, even if they are at the same detector
        # (in that case, there is no correlation between photon and memory qubit)
        dm += self._pf2 * total_dep_prob * classically_correlated

        # If zero or one photons arrive at the detector, and dark counts create a false positive,
        # this gives no information about the state at all.
        # We then assign a maximally mixed state.
        # Furthermore, if depolarization took place, the final state is always maximally mixed.
        dm += ((self._pt + self._pf1 + self._pf2) * (1 - total_dep_prob) + self._pf3 + self._pf4) * maxmixed

        # Finally, we normalize the density matrix.
        # If no mistakes were made, the normalization constant should be the sum of the probabilities
        # of the different cases (which is the success probability).
        dm = dm / (self._pt + self._pf1 + self._pf2 + self._pf3 + self._pf4)

        return dm


def success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(factory, **kwargs):
    """Extract the success probability and fidelity from a heralded state delivery sampler factory.

    Parameters
    ----------
    factory : :class:`netsquid_magic.state_delivery_sampler.HeraldedStateDeliverySamplerFactory`
        Class (so not an object) that inherits from heralded state delivery sampler factory
        to extract success probability and fidelity from.
        Parameters that need to be passed to the constructor of the class should be passed as key word arguments
        to this function.

    Returns
    -------
    float
        Success probability.
    float
        Average fidelity of the delivered state to the Bell state indicated by the delivered label.

    """
    ns.set_qstate_formalism(ns.QFormalism.DM)
    delivery_sampler = factory()
    state_sampler, success_prob = delivery_sampler._func_delivery(**kwargs)
    fids = []
    probs = []
    for (state, prob, label) in state_sampler.get_leaves():
        qubits = qapi.create_qubits(num_qubits=2)
        qapi.assign_qstate(qubits=qubits, qrepr=state)
        fids.append(qapi.fidelity(qubits, bell_states[label], squared=True))
        probs.append(prob)
    fid = sum([f * p for f, p in zip(fids, probs)])
    return success_prob, fid

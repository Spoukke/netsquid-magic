import unittest
from unittest import mock

from netsquid.components.component import Component
from netsquid.nodes.node import Node

from netsquid_magic.distributor_server import get_magic_distributor
from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import IStateDeliverySamplerFactory


class TestDistributorServer(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.nodes = [Node("nodeA", 0), Node("nodeB", 1)]
        cls.component1 = Component('component_1')
        cls.component2 = Component('component_2')
        cls.delivery_sampler_factory = IStateDeliverySamplerFactory()

        cls.mock_module = 'netsquid_magic.magic_distributor.MagicDistributor.get_qmemories_from_nodes'

    def test_get(self):
        # No argument
        with self.assertRaises(TypeError):
            get_magic_distributor()

        # No delivery sampler factory class
        with self.assertRaises(TypeError):
            get_magic_distributor(component=self.component1)

        # Not a component
        with self.assertRaises(TypeError):
            get_magic_distributor(
                nodes=self.nodes,
                component=1,
                delivery_sampler_factory=self.delivery_sampler_factory)

        # Not a  node
        with self.assertRaises(TypeError):
            get_magic_distributor(
                nodes=[self.nodes[0], 1],
                component=1,
                delivery_sampler_factory=self.delivery_sampler_factory)

        # Not a delivery sampler class
        with self.assertRaises(TypeError):
            get_magic_distributor(
                nodes=self.nodes,
                component=self.component1,
                magic_distributor_class="abc")

    def test_get_same(self):
        with mock.patch(self.mock_module) as mock_get_qmems:
            mock_get_qmems.return_value = []
            # Get the same distributors
            distributor1 = get_magic_distributor(
                nodes=self.nodes,
                component=self.component1,
                delivery_sampler_factory=self.delivery_sampler_factory)
            distributor2 = get_magic_distributor(
                nodes=self.nodes,
                component=self.component1,
                delivery_sampler_factory=self.delivery_sampler_factory)

            self.assertIs(distributor1, distributor2)
            self.assertIsInstance(distributor1, MagicDistributor)
            self.assertIsInstance(distributor2, MagicDistributor)

    def test_get_different(self):
        with mock.patch(self.mock_module) as mock_get_qmems:
            mock_get_qmems.return_value = []
            # Get differerent distributors
            distributor1 = get_magic_distributor(
                nodes=self.nodes,
                component=self.component1,
                delivery_sampler_factory=self.delivery_sampler_factory)
            distributor2 = get_magic_distributor(
                nodes=self.nodes,
                component=self.component2,
                delivery_sampler_factory=self.delivery_sampler_factory)

            self.assertIsNot(distributor1, distributor2)
            self.assertIsInstance(distributor1, MagicDistributor)
            self.assertIsInstance(distributor2, MagicDistributor)


if __name__ == '__main__':
    unittest.main()

import unittest
from collections import defaultdict

from pydynaa import Entity, EventType, EventHandler

from netsquid_magic.link_layer import (
    LinkLayerService,
    MagicLinkLayerProtocol,
    SingleClickTranslationUnit,
)
from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqRemoteStatePrep,
    ReqReceive,
    ResCreateAndKeep,
    ResMeasureDirectly,
    ResError,
)
from netsquid_magic.magic_distributor import SingleClickMagicDistributor
from netsquid.util.simtools import sim_reset, sim_run
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
import netsquid as ns

FREE_QUBIT_DELAY = 10000


class PostPoner(Entity):
    def __init__(self):
        super().__init__()
        self._ev = EventType('EVENT', 'event')

    def post_pone(self, delay, function, *args, **kwargs):
        handler = EventHandler(lambda Event: function(*args, **kwargs))
        self._schedule_after(delay, self._ev)
        self._wait_once(handler=handler, entity=self, event_type=self._ev)


class TestLinkLayer(unittest.TestCase):
    def setUp(self):
        # Reset messages
        self.messages = defaultdict(lambda: defaultdict(list))

        # post poner
        self.post_poner = PostPoner()

        # Defaults
        self.nr_qubits_A = 1
        self.nr_qubits_B = 1
        self.length_A = 25
        self.length_B = 25
        self.speed_of_light_A = 200000
        self.speed_of_light_B = 200000
        self.expected_delay = 2 * self.length_A / self.speed_of_light_A * 1E9
        self.handlerA = self.handleA
        self.handlerB = self.handleB

    def tearDown(self):
        for messages in self.messages.values():
            assert len(messages[ResError]) == 0

    def setup(self):
        sim_reset()

        ns.set_qstate_formalism(ns.QFormalism.DM)

        self.qmemA = QuantumProcessor("qmemA", self.nr_qubits_A)
        self.qmemB = QuantumProcessor("qmemB", self.nr_qubits_B)
        self.nodeA = Node("nodeA", 0, qmemory=self.qmemA)
        self.nodeB = Node("nodeB", 1, qmemory=self.qmemB)
        self.nodes = [self.nodeA, self.nodeB]

        self.magic_distributor = SingleClickMagicDistributor(self.nodes, length_A=self.length_A, length_B=self.length_B,
                                                             speed_of_light_A=self.speed_of_light_A,
                                                             speed_of_light_B=self.speed_of_light_B)

        self.translation_unit = SingleClickTranslationUnit()

        self.magic_protocol = MagicLinkLayerProtocol(nodes=self.nodes, magic_distributor=self.magic_distributor, translation_unit=self.translation_unit)

        self.service_interfaceA = LinkLayerService(self.nodeA, magic=True, magic_protocol=self.magic_protocol, reaction_handler=self.handlerA)
        self.service_interfaceB = LinkLayerService(self.nodeB, magic=True, magic_protocol=self.magic_protocol, reaction_handler=self.handlerB)

        self.service_interfaceA.start()
        self.service_interfaceB.start()

        # Setup rules to recv
        recv_requestA = ReqReceive(remote_node_id=self.nodeB.ID)
        self.service_interfaceA.put(recv_requestA)
        recv_requestB = ReqReceive(remote_node_id=self.nodeA.ID)
        self.service_interfaceB.put(recv_requestB)

    def handleA(self, msg):
        self.messages[self.nodeA.ID][type(msg)].append(msg)
        if isinstance(msg, ResCreateAndKeep):
            # Free qubit after some time
            self.post_poner.post_pone(FREE_QUBIT_DELAY, self.free_qubit, self.qmemA, msg.logical_qubit_id)

    def handleB(self, msg):
        self.messages[self.nodeB.ID][type(msg)].append(msg)
        if isinstance(msg, ResCreateAndKeep):
            # Free qubit after some time
            self.post_poner.post_pone(FREE_QUBIT_DELAY, self.free_qubit, self.qmemB, msg.logical_qubit_id)

    def free_qubit(self, qmem, logical_qubit_id):
        qmem.mem_positions[logical_qubit_id].in_use = False

    def test_one_k_one_pair(self):
        num_pairs = 1
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqCreateAndKeep
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            self.assertEqual(len(ok_k_messages), num_pairs * num_requests)

    def test_one_m_one_pair(self):
        num_pairs = 1
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqMeasureDirectly
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_m_messages), num_pairs * num_requests)

    def test_one_r_one_pair(self):
        num_pairs = 1
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqRemoteStatePrep
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            with self.assertRaises(NotImplementedError):
                self.service_interfaceA.put(create_request)

    def test_one_k_three_pair(self):
        num_pairs = 3
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqCreateAndKeep
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            self.assertEqual(len(ok_k_messages), num_pairs * num_requests)

    def test_one_m_three_pair(self):
        num_pairs = 3
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqMeasureDirectly
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_m_messages), num_pairs * num_requests)

    def test_two_k_two_pair(self):
        num_pairs = 3
        num_requests = 1
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqCreateAndKeep
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            self.assertEqual(len(ok_k_messages), num_pairs * num_requests)

    def test_two_m_two_pair(self):
        num_pairs = 2
        num_requests = 2
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        request_type = ReqMeasureDirectly
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_m_messages), num_pairs * num_requests)

    def test_small_memory_k(self):
        num_pairs = 2
        num_requests = 2
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = 1
        self.setup()

        request_type = ReqCreateAndKeep
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()
        # Make sure that link layer waited for qubit to be freed
        assert ns.sim_time() >= num_pairs * num_requests * FREE_QUBIT_DELAY

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            self.assertEqual(len(ok_k_messages), num_pairs * num_requests)

    def test_small_memory_m(self):
        num_pairs = 2
        num_requests = 2
        self.nr_qubits_A = num_pairs * num_requests
        self.nr_qubits_B = 1
        self.setup()

        request_type = ReqMeasureDirectly
        minimum_fidelity = 0.9

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)

        sim_run()
        # Make sure that link layer did not have to wait,
        # since m requests don't need to be freed
        assert ns.sim_time() < 2 * (FREE_QUBIT_DELAY + self.expected_delay)

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_m_messages), num_pairs * num_requests)

    def test_small_memory_first_m_then_k(self):
        self.nr_qubits_A = 2
        self.nr_qubits_B = 1
        self.setup()

        minimum_fidelity = 0.9

        create_request = ReqMeasureDirectly(
            remote_node_id=self.nodeB.ID,
            number=1,
            minimum_fidelity=minimum_fidelity,
        )
        self.service_interfaceA.put(create_request)
        create_request = ReqCreateAndKeep(
            remote_node_id=self.nodeB.ID,
            number=1,
            minimum_fidelity=minimum_fidelity,
        )
        self.service_interfaceA.put(create_request)

        sim_run()
        # Make sure that link layer waited for qubit to be freed
        assert ns.sim_time() >= FREE_QUBIT_DELAY
        # Both not to long, since M request don't need to be freed
        assert ns.sim_time() < 2 * (FREE_QUBIT_DELAY + self.expected_delay)

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_k_messages), 1)
            self.assertEqual(len(ok_m_messages), 1)

    def test_small_memory_first_k_then_m(self):
        self.nr_qubits_A = 2
        self.nr_qubits_B = 1
        self.setup()

        minimum_fidelity = 0.9

        create_request = ReqCreateAndKeep(
            remote_node_id=self.nodeB.ID,
            number=1,
            minimum_fidelity=minimum_fidelity,
        )
        self.service_interfaceA.put(create_request)
        create_request = ReqMeasureDirectly(
            remote_node_id=self.nodeB.ID,
            number=1,
            minimum_fidelity=minimum_fidelity,
        )
        self.service_interfaceA.put(create_request)

        sim_run()
        # Make sure that link layer waited for qubit to be freed
        assert ns.sim_time() >= FREE_QUBIT_DELAY
        # Both not to long, since M request don't need to be freed
        assert ns.sim_time() < 2 * (FREE_QUBIT_DELAY + self.expected_delay)

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            ok_m_messages = messages[ResMeasureDirectly]
            self.assertEqual(len(ok_k_messages), 1)
            self.assertEqual(len(ok_m_messages), 1)

    def test_requests_both_nodes(self):
        num_pairs = 1
        num_requests = 2
        self.nr_qubits_A = 2 * num_requests * num_pairs
        self.nr_qubits_B = self.nr_qubits_A
        self.setup()

        minimum_fidelity = 0.9
        request_type = ReqCreateAndKeep

        for _ in range(num_requests):
            create_request = request_type(
                remote_node_id=self.nodeB.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceA.put(create_request)
            create_request = request_type(
                remote_node_id=self.nodeA.ID,
                number=num_pairs,
                minimum_fidelity=minimum_fidelity,
            )
            self.service_interfaceB.put(create_request)

        sim_run()

        for node in self.nodes:
            messages = self.messages[node.ID]
            ok_k_messages = messages[ResCreateAndKeep]
            self.assertEqual(len(ok_k_messages), 2 * num_pairs * num_requests)

    def test_reserve_mem_position(self):
        self.nr_qubits_A = 2
        self.nr_qubits_B = 2
        self.setup()
        # set memory position to None explicitly
        self.qmemA.put(None, positions=1)
        # check if all positions can be reserved properly
        for pos in range(2):
            self.magic_protocol._reserve_memory_position(qmemory=self.qmemA, memory_position=pos)
            self.magic_protocol._reserve_memory_position(qmemory=self.qmemB, memory_position=pos)

            assert self.qmemA.mem_positions[pos].in_use
            assert self.qmemB.mem_positions[pos].in_use


if __name__ == '__main__':
    unittest.main()

import logging
import unittest
from netsquid_physlayer.heralded_connection import MiddleHeraldedConnection, HeraldedConnection, \
    SameChannelsHeraldedConnection
from netsquid import BellIndex
from netsquid.components import INSTR_EMIT, INSTR_INIT
from netsquid_physlayer.detectors import BSMOutcome
from netsquid_magic.magic_distributor import MagicDistributor, DoubleClickMagicDistributor, \
    SingleClickMagicDistributor, DepolariseWithFailureMagicDistributor
from collections import namedtuple
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.connections import Connection
from netsquid.nodes.node import Node
from netsquid.qubits.qubit import Qubit
from netsquid_magic.state_delivery_sampler import HeraldedStateDeliverySamplerFactory
import numpy as np
import netsquid as ns
from netsquid.qubits.ketstates import b00, s00, s11, s1
from netsquid.qubits.state_sampler import StateSampler


class FakeDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):

    def __init__(self):
        self.obj = StateSampler(qreprs=[s00, s11], probabilities=[0.4, 0.6]), 0.5
        super().__init__(func_delivery=lambda p, **kwargs: self.obj)


class FakeDeliverySamplerFactorySingleState(HeraldedStateDeliverySamplerFactory):

    def __init__(self, use_bell_state=True):
        state = b00 if use_bell_state else s11
        self.obj = StateSampler(qreprs=[state], probabilities=[1.0]), 0.5
        super().__init__(func_delivery=lambda p, **kwargs: self.obj)


class FakeDeliverySamplerFactoryMultiQubitState(HeraldedStateDeliverySamplerFactory):

    def __init__(self):
        # Create the 8-qubit state |00000000>
        eight_qubit_state = np.zeros([2 ** 8, 1], dtype=complex)
        eight_qubit_state[0] = 1
        self.obj = StateSampler([eight_qubit_state], probabilities=[1]), 1
        super().__init__(func_delivery=lambda p, **kwargs: self.obj)


FakeNode = namedtuple("NodeA", "qmemory")


class FakeConnection(Connection):

    def __init__(self, nodes):
        self.nodeA, self.nodeB = nodes


class MD(MagicDistributor):

    states_added = 0
    labels_added = 0

    def _handle_state_delivery(self, node_delivery, event):
        super()._handle_state_delivery(
            node_delivery=node_delivery,
            event=event,
        )
        self.states_added += 1

    def _handle_label_delivery(self, node_delivery, event):
        super()._handle_label_delivery(node_delivery=node_delivery, event=event)
        self.labels_added += 1


class TestMagicDistributor(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        self.port_name = "ENT_LABEL"
        self.state_delay = 42
        self.label_delay = 10

        nodeA = Node("nodeA", 0, qmemory=QuantumProcessor(name="A", num_positions=20), port_names=[self.port_name])
        nodeB = Node("nodeB", 1, qmemory=QuantumProcessor(name="B", num_positions=20), port_names=[self.port_name])
        self.nodes = [nodeA, nodeB]

        nodeC = Node("nodeC", 2, qmemory=QuantumProcessor(name="C", num_positions=20), port_names=[self.port_name])
        nodeD = Node("nodeD", 3, qmemory=QuantumProcessor(name="D", num_positions=20), port_names=[self.port_name])
        self.other_nodes = [nodeC, nodeD]

        self.connection = FakeConnection(nodes=self.nodes)
        self.md = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                     nodes=self.nodes,
                     component=self.connection,
                     state_delay=self.state_delay,
                     label_delay=self.label_delay)

        self.parameters = {"p": 0.9, "a": 42}
        self.cycle_time = 12
        self.memory_positions = {self.nodes[0].ID: 2, self.nodes[1].ID: 3}

    def test_not_nodes(self):
        nodes = [FakeNode(2), FakeNode(2)]
        with self.assertRaises(NotImplementedError):
            MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
               nodes=nodes,
               component=FakeConnection(nodes=nodes))

    def test_add_delivery(self):
        """Check that state is added when add_delivery method is called."""
        self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        self.assertEqual(self.md.states_added, 0)
        ns.sim_run()
        self.assertEqual(self.md.states_added, 2)

    def abort_all_wrapper(self, delivery_event):
        self.md.abort_all_delivery()

    def abort_before_delivery(self, abort_function):
        """Check that delivery is aborted if abort_delivery called before the state is added."""

        delivery_event = self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time,
                                              **self.parameters)
        self.assertEqual(self.md.states_added, 0)
        self.assertEqual(self.md.labels_added, 0)

        node_delivery = self.md.peek_node_delivery(delivery_event)
        expected_state_delivery_time = node_delivery.delivery.sample.delivery_duration + self.state_delay
        ns.sim_run(duration=expected_state_delivery_time - 1)
        self.assertEqual(self.md.states_added, 0)  # state still not added because it's before state delay
        self.assertEqual(self.md.labels_added, 0)

        abort_function(delivery_event)

        ns.sim_run()
        self.assertEqual(self.md.states_added, 0)  # state not added because delivery was aborted
        self.assertEqual(self.md.labels_added, 0)

    def test_abort_before_delivery_abort_delivery(self):
        self.abort_before_delivery(abort_function=self.md.abort_delivery)

    def test_abort_before_delivery_abort_all(self):
        self.abort_before_delivery(abort_function=self.abort_all_wrapper)

    def abort_after_delivery(self, abort_function):
        """Check that if abort_delivery is called after state is added, the state stays added."""
        delivery_event = self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time,
                                              **self.parameters)
        self.assertEqual(self.md.states_added, 0)
        self.assertEqual(self.md.labels_added, 0)

        ns.sim_run()
        self.assertEqual(self.md.states_added, 2)  # states are now added
        self.assertEqual(self.md.labels_added, 2)

        abort_function(delivery_event)

        ns.sim_run()
        self.assertEqual(self.md.states_added, 2)  # states should stay added
        self.assertEqual(self.md.labels_added, 2)

    def test_abort_after_delivery_abort_delivery(self):
        self.abort_after_delivery(abort_function=self.md.abort_delivery)

    def test_abort_after_delivery_abort_all(self):
        self.abort_after_delivery(abort_function=self.abort_all_wrapper)

    def abort_between_state_and_label_delivery(self, abort_function):
        delivery_event = self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time,
                                              **self.parameters)
        self.assertEqual(self.md.states_added, 0)
        self.assertEqual(self.md.labels_added, 0)

        node_delivery = self.md.peek_node_delivery(delivery_event)
        expected_state_delivery_time = node_delivery.delivery.sample.delivery_duration + self.state_delay
        sim_duration = expected_state_delivery_time + self.label_delay / 2
        ns.sim_run(sim_duration)  # after state delivery, before label delivery
        self.assertEqual(self.md.states_added, 2)
        self.assertEqual(self.md.labels_added, 0)

        abort_function(delivery_event)

        ns.sim_run(duration=self.label_delay)  # state and label would now be delivered
        self.assertEqual(self.md.states_added, 2)
        self.assertEqual(self.md.labels_added, 0)

    def test_abort_between_state_and_label_abort_delivery(self):
        self.abort_between_state_and_label_delivery(abort_function=self.md.abort_delivery)

    def test_abort_between_state_and_label_abort_all(self):
        self.abort_between_state_and_label_delivery(abort_function=self.abort_all_wrapper)

    def test_adding_state(self):
        ns.set_qstate_formalism(ns.QFormalism.KET)
        # TODO also test for the case where the FakeDeliverySamplerFactory outputs a sampler for DM instead of just KET

        connection = FakeConnection(nodes=self.nodes)
        md = MD(delivery_sampler_factory=FakeDeliverySamplerFactorySingleState(use_bell_state=True),
                nodes=self.nodes,
                component=connection)

        md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        ns.sim_run()

        [qubit1] = connection.nodeA.qmemory.peek(self.memory_positions[self.nodes[0].ID])
        [qubit2] = connection.nodeB.qmemory.peek(self.memory_positions[self.nodes[1].ID])
        self.assertTrue(np.isclose(ns.qubits.qubitapi.fidelity([qubit1, qubit2], b00), 1))

    def test_apply_noise_after_adding_state(self):
        ns.set_qstate_formalism(ns.QFormalism.KET)

        connection = FakeConnection(nodes=self.nodes)

        class MDWithApplyNoise(MD):

            def _apply_noise(self, delivery, quantum_memory, positions):
                # act on each of the memory positions with Pauli X
                for pos in positions:
                    [qubit] = quantum_memory.peek(pos)
                    if qubit is not None:
                        ns.qubits.qubitapi.operate(qubit, ns.qubits.operators.X)

        md = MDWithApplyNoise(delivery_sampler_factory=FakeDeliverySamplerFactorySingleState(use_bell_state=False),
                              nodes=self.nodes,
                              component=connection)

        for node in [connection.nodeA, connection.nodeB]:
            node.qmemory.put(qubits=ns.qubits.qubitapi.create_qubits(1), positions=0)

        md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        ns.sim_run()

        # check if delivered qubits were acted on with Pauli X
        [qubit1] = connection.nodeA.qmemory.peek(self.memory_positions[self.nodes[0].ID])
        [qubit2] = connection.nodeB.qmemory.peek(self.memory_positions[self.nodes[1].ID])
        self.assertTrue(np.isclose(ns.qubits.qubitapi.fidelity([qubit1, qubit2], s00), 1))

        # check if non-participating qubits were acted on with Pauli X
        for node in [connection.nodeA, connection.nodeB]:
            [qubit] = node.qmemory.peek(0)
            self.assertTrue(np.array_equal(qubit.qstate.qrepr.ket, s1))

    def test_adding_callback(self):
        """Check that callback function is called when delivery is added."""
        self.md.callback_called = False

        def set_called_to_true(event, md):
            md.callback_called = True

        self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        self.md.add_callback(lambda event, md=self.md: set_called_to_true(event=event, md=md))

        self.assertFalse(self.md.callback_called)
        ns.sim_run()
        self.assertTrue(self.md.callback_called)

    def _test_state_and_label_delay(self, state_delay, label_delay, expected_state_duration, expected_label_duration):

        md = MD(
            delivery_sampler_factory=FakeDeliverySamplerFactory(),
            nodes=self.nodes,
            component=FakeConnection(nodes=self.nodes),
            state_delay=state_delay,
            label_delay=label_delay,
        )

        start_time = ns.sim_time()
        node_deliveries = {}

        def assert_state_delivery(event, md):
            time = ns.sim_time()
            node_delivery = md.peek_node_delivery(event)
            node_deliveries[node_delivery.node_id] = node_delivery

            assert md._get_total_state_delay(node_delivery) == expected_state_duration(node_delivery)

            self.assertAlmostEqual(time, start_time + expected_state_duration(node_delivery))

        def assert_label_delivery_factory(node):

            def assert_label_delivery(msg):
                time = ns.sim_time()
                node_delivery = node_deliveries[node.ID]
                label = msg.items[0]

                self.assertEqual(label[0], 'success')
                self.assertAlmostEqual(time, start_time + expected_label_duration(node_delivery))

            return assert_label_delivery

        for node in self.nodes:
            port = node.ports[self.port_name]
            port.bind_input_handler(assert_label_delivery_factory(node), tag_meta=True)

        ports = {node.ID: self.port_name for node in self.nodes}
        md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters, ports=ports)
        md.add_callback(lambda event, md=md: assert_state_delivery(event=event, md=md))

        ns.sim_run()

    def test_constant_state_and_label_delay(self):
        state_delay = 43
        label_delay = 32

        def expected_state_duration(node_delivery):
            return node_delivery.delivery.sample.delivery_duration + state_delay

        def expected_label_duration(node_delivery):
            return expected_state_duration(node_delivery) + label_delay

        self._test_state_and_label_delay(state_delay=state_delay, label_delay=label_delay,
                                         expected_state_duration=expected_state_duration,
                                         expected_label_duration=expected_label_duration)

    def test_different_state_delays(self):

        state_delay = {self.nodes[0].ID: 100, self.nodes[1].ID: 200}
        label_delay = 10

        def expected_state_duration(node_delivery):
            return node_delivery.delivery.sample.delivery_duration + state_delay[node_delivery.node_id]

        def expected_label_duration(node_delivery):
            return expected_state_duration(node_delivery) + label_delay

        self._test_state_and_label_delay(state_delay=state_delay, label_delay=label_delay,
                                         expected_state_duration=expected_state_duration,
                                         expected_label_duration=expected_label_duration)

    def test_different_label_delays(self):

        state_delay = 100
        label_delay = {self.nodes[0].ID: 10, self.nodes[1].ID: 20}

        def expected_state_duration(node_delivery):
            return node_delivery.delivery.sample.delivery_duration + state_delay

        def expected_label_duration(node_delivery):
            return expected_state_duration(node_delivery) + label_delay[node_delivery.node_id]

        self._test_state_and_label_delay(state_delay=state_delay, label_delay=label_delay,
                                         expected_state_duration=expected_state_duration,
                                         expected_label_duration=expected_label_duration)

    def test_different_delays(self):

        state_delay = {self.nodes[0].ID: 100, self.nodes[1].ID: 200}
        label_delay = {self.nodes[0].ID: 10, self.nodes[1].ID: 20}

        def expected_state_duration(node_delivery):
            return node_delivery.delivery.sample.delivery_duration + state_delay[node_delivery.node_id]

        def expected_label_duration(node_delivery):
            return expected_state_duration(node_delivery) + label_delay[node_delivery.node_id]

        self._test_state_and_label_delay(state_delay=state_delay, label_delay=label_delay,
                                         expected_state_duration=expected_state_duration,
                                         expected_label_duration=expected_label_duration)

    def test_multi_qubit_state_delivery(self):
        md = MD(delivery_sampler_factory=FakeDeliverySamplerFactoryMultiQubitState(),
                nodes=self.nodes,
                component=FakeConnection(nodes=self.nodes),
                num_qubits_per_memory=4)
        memory_positions = {self.nodes[0].ID: list(range(4)), self.nodes[1].ID: list(range(4))}
        md.add_delivery(memory_positions=memory_positions, cycle_time=self.cycle_time, **self.parameters)
        ns.sim_run()
        [self.assertTrue(type(item) == Qubit) for node in self.nodes for item in node.qmemory.peek(list(range(4)))]

    def test_request_filed_by_one_node(self):
        """Check that if request is only filed by one node, the state is not added."""
        # add request by only one node
        self.parameters = {"p": 0.9, "a": 42, "cycle_time": 12}
        self.md.add_pair_request(self.nodes[0].ID, self.nodes[1].ID, 1, delivery_params=self.parameters)

        # check that no state was added
        self.assertEqual(self.md.states_added, 0)
        ns.sim_run()
        self.assertEqual(self.md.states_added, 0)

    def test_request_filed_by_both_nodes(self):
        """Check that state is added when request is filed by both nodes."""
        # add request from both nodes
        self.parameters = {"p": 0.9, "a": 42, "cycle_time": 12}
        self.md.add_pair_request(self.nodes[0].ID, self.nodes[1].ID, 2, delivery_params=self.parameters)
        self.md.add_pair_request(self.nodes[1].ID, self.nodes[0].ID, 3, delivery_params=self.parameters)

        # check that state was added
        self.assertEqual(self.md.states_added, 0)
        ns.sim_run()
        self.assertEqual(self.md.states_added, 2)

    def test_merge_other_magic_distributor_success(self):
        """Check that correctly merging mds gives no error and the result class variables have correct length."""
        # set up other md
        other = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                   nodes=self.other_nodes,
                   num_qubits_per_memory=2)

        self.md.merge_magic_distributor(other)

        self.assertTrue(len(self.md.delivery_sampler_factory), 2)
        self.assertTrue(len(self.md.nodes), 2)
        self.assertEqual(self.md.nodes, [self.nodes, self.other_nodes])
        self.assertTrue(len(self.md.component), 2)
        self.assertEqual(self.md.component[1], None)
        self.assertTrue(len(self.md.fixed_delivery_parameters), 2)
        self.assertEqual(self.md._num_qubits_per_memory[0], 1)
        self.assertEqual(self.md._num_qubits_per_memory[1], 2)

    def test_merge_other_magic_distributor_error(self):
        """Check that magic distributor is not merged with something that is not a magic distributor."""
        with self.assertRaises(TypeError):
            self.md.merge_magic_distributor("this is not a magic distributor")

    def test_delivery_for_merged(self):
        """Check that delivery can be added after magic distributors are merged."""
        # set up other md
        other = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                   nodes=self.other_nodes,
                   num_qubits_per_memory=2)

        self.md.merge_magic_distributor(other)

        self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        self.assertEqual(self.md.states_added, 0)
        ns.sim_run()
        self.assertEqual(self.md.states_added, 2)

    def test_custom_labels(self):
        """Check that setting up custom labels works."""
        md = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                nodes=self.nodes,
                custom_labels=["label1", "label2"])
        self.assertEqual(md.fixed_delivery_parameters[0]['custom_labels'], ["label1", "label2"])

    def test_qmemories(self):
        """Check that calling `get_qmemories_from_nodes` does not work on merged magic distributor."""
        # set up other md
        other = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                   nodes=self.other_nodes,
                   num_qubits_per_memory=2)

        self.md.merge_magic_distributor(other)

        with self.assertRaises(ValueError):
            self.md.get_qmemories_from_nodes()

    def test_update_delays(self):
        """Check that updating delays for merged magic distributor only updates intended values."""
        md = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                nodes=self.nodes,
                state_delay=10,
                label_delay=20)

        # set up other md
        other = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                   nodes=self.other_nodes,
                   state_delay=30,
                   label_delay=40)

        md.merge_magic_distributor(other)

        self.assertEqual(md._state_delay, [{node.ID: 10 for node in self.nodes},
                                           {node.ID: 30 for node in self.other_nodes}])
        self.assertEqual(md._label_delay, [{node.ID: 20 for node in self.nodes},
                                           {node.ID: 40 for node in self.other_nodes}])

        # update label delay for "first" magic distributor and state delay for "second" magic distributor
        md.fixed_delivery_parameters = [{'label_delay': 50}, {'state_delay': 60}]
        md._update_delays()
        self.assertEqual(md._state_delay, [{node.ID: 10 for node in self.nodes},
                                           {node.ID: 60 for node in self.other_nodes}])
        self.assertEqual(md._label_delay, [{node.ID: 50 for node in self.nodes},
                                           {node.ID: 40 for node in self.other_nodes}])

        md.fixed_delivery_parameters[0] = {"state_delay": {self.nodes[0].ID: 70, self.nodes[1].ID: 80},
                                           "label_delay": {self.nodes[0].ID: 90, self.nodes[1].ID: 100}}
        md._update_delays()

        self.assertEqual(md._state_delay[0][self.nodes[0].ID], 70)
        self.assertEqual(md._state_delay[0][self.nodes[1].ID], 80)
        self.assertEqual(md._label_delay[0][self.nodes[0].ID], 90)
        self.assertEqual(md._label_delay[0][self.nodes[1].ID], 100)

    def test_delivery_from_different_mds(self):
        """Check that delivery where not all nodes originate from the same magic distributor is rejected."""
        # set up other md
        other = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                   nodes=self.other_nodes)

        self.md.merge_magic_distributor(other)
        memory_positions = {0: 2, 2: 3}

        with self.assertRaises(ValueError):
            self.md.add_delivery(memory_positions)

    def test_delivery_for_less_nodes(self):
        """Check that delivery for less nodes than are in the MD is rejected. """
        md = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                nodes=self.nodes+self.other_nodes)
        memory_positions = {0: 2, 1: 2, 2: 2}

        with self.assertRaises(ValueError):
            md.add_delivery(memory_positions)

    def test_delivery_for_wrong_number_of_qmemories(self):
        """Check that delivery for nodes with incorrect number of qmemories is rejected."""
        memory_positions = {0: [2, 1], 1: 2}  # node with ID 0 only has 1 qmemory

        with self.assertRaises(ValueError):
            self.md.add_delivery(memory_positions)

    def test_set_skip_rounds(self):
        """Check the functionality of `set_skip_rounds`."""
        self.assertTrue(self.md._skip_rounds[0])
        self.md.set_skip_rounds(False)
        self.assertFalse(self.md._skip_rounds[0])

    def test_delivery_on_both_sides(self):
        nodes = [Node("Alice"), Node("Bob"), Node("Charlie")]
        nodes[0].qmemory = QuantumProcessor("processor_alice", 1)
        nodes[1].qmemory = QuantumProcessor("processor_bob", 2)
        nodes[2].qmemory = QuantumProcessor("processor_charlie", 1)
        md1 = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                 nodes=[nodes[0], nodes[1]])
        md2 = MD(delivery_sampler_factory=FakeDeliverySamplerFactory(),
                 nodes=[nodes[1], nodes[2]])
        md1.add_delivery(memory_positions={nodes[0].ID: 0,
                                           nodes[1].ID: 0},
                         cycle_time=1000,
                         **self.parameters)
        md2.add_delivery(memory_positions={nodes[1].ID: 1,
                                           nodes[2].ID: 0},
                         cycle_time=1000,
                         **self.parameters)

    def assert_mem_position_in_use(self, in_use):
        """Assert whether memory positions are in use or not.

        Parameters
        ----------
        in_use : bool
            Whether to assert memory positions are in use (True) or whether they are not (False).

        """
        for node in self.nodes:
            try:
                # md.add_delivery puts the memory positions in a list
                memory_position = node.qmemory.mem_positions[self.memory_positions[node.ID][0]]
            except TypeError:
                memory_position = node.qmemory.mem_positions[self.memory_positions[node.ID]]
            assert memory_position.in_use == in_use

    def test_memory_positions_in_use(self):
        """test whether memory positions are correctly put to "in use" during entanglement generation"""

        self.assert_mem_position_in_use(in_use=False)
        self.md.add_delivery(memory_positions=self.memory_positions, cycle_time=self.cycle_time, **self.parameters)
        ns.sim_run(duration=1)
        self.assert_mem_position_in_use(in_use=True)
        ns.sim_run()
        self.assert_mem_position_in_use(in_use=True)  # memory position stays in use until someone "consumes" the qubit

    def test_get_label(self):
        event = self.md.add_delivery(memory_positions=self.memory_positions,
                                     cycle_time=self.cycle_time,
                                     **self.parameters)
        label = self.md.get_label(event)
        assert label is not None
        latest_label = self.md.get_label(None)
        assert latest_label == label
        ns.sim_run()
        # make sure label can still be obtained after delivery has completed
        label_after_delivery = self.md.get_label(event)
        assert label_after_delivery == label
        latest_label = self.md.get_label(None)
        assert latest_label == label


class TestSingleClickMagicDistributor(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        self.length = 10
        self.dark_count_probability = .01
        self.visibility = .95
        self.p_loss_init = .9
        self.p_loss_length = .3
        self.detector_efficiency = .9
        self.num_resolving = False

        self.nodes = [Node("Alice"), Node("Bob")]

        for node in self.nodes:
            node.add_ports(["to_heralded_connection"])
            node.qmemory = QuantumProcessor(name=f"test_processor_{node.name}")

    def initialize_components(self):
        """Initialize heralded connection with parameters of this test class."""
        heralded_connection = MiddleHeraldedConnection(name="test_heralded_connection", length=self.length,
                                                       p_loss_init=self.p_loss_init,
                                                       p_loss_length=self.p_loss_length,
                                                       dark_count_probability=self.dark_count_probability,
                                                       detector_efficiency=self.detector_efficiency,
                                                       visibility=self.visibility,
                                                       num_resolving=self.num_resolving)
        return heralded_connection

    def assert_parameters_correct(self):
        assert self.md.fixed_delivery_parameters[0]["dark_count_probability"] == self.dark_count_probability
        assert self.md.fixed_delivery_parameters[0]["visibility"] == self.visibility
        assert self.md.fixed_delivery_parameters[0]["p_loss_init_A"] == self.p_loss_init
        assert self.md.fixed_delivery_parameters[0]["p_loss_init_B"] == self.p_loss_init
        assert self.md.fixed_delivery_parameters[0]["detector_efficiency"] == self.detector_efficiency
        assert self.md.fixed_delivery_parameters[0]["p_loss_length_A"] == self.p_loss_length
        assert self.md.fixed_delivery_parameters[0]["p_loss_length_B"] == self.p_loss_length
        assert self.md.fixed_delivery_parameters[0]["num_resolving"] == self.num_resolving

    def test_reading_from_component(self):
        """MD should have parameters that were read out from the heralded connection."""
        self.md = SingleClickMagicDistributor(nodes=self.nodes,
                                              heralded_connection=self.initialize_components())
        self.assert_parameters_correct()

    def test_overwriting_component_parameters(self):
        """Specifying parameters overwrites parameters read out from the heralded connection."""
        self.md = SingleClickMagicDistributor(nodes=self.nodes,
                                              heralded_connection=self.initialize_components(),
                                              p_loss_init_A=.8, p_loss_init_B=.8,
                                              p_loss_length_A=.2, p_loss_length_B=.2,
                                              detector_efficiency=.8, dark_count_probability=.1,
                                              visibility=.6, num_resolving=True)
        assert self.md.fixed_delivery_parameters[0]["dark_count_probability"] == .1
        assert self.md.fixed_delivery_parameters[0]["visibility"] == .6
        assert self.md.fixed_delivery_parameters[0]["p_loss_init_A"] == .8
        assert self.md.fixed_delivery_parameters[0]["p_loss_init_B"] == .8
        assert self.md.fixed_delivery_parameters[0]["detector_efficiency"] == .8
        assert self.md.fixed_delivery_parameters[0]["p_loss_length_A"] == .2
        assert self.md.fixed_delivery_parameters[0]["p_loss_length_B"] == .2
        assert self.md.fixed_delivery_parameters[0]["num_resolving"]

    def test_reading_uniform_alpha_from_node(self):
        """SingleClickMD should read out the bright-state parameter if it's defined uniformly for the nodes."""
        self.nodes[0].add_property("bright_state_param", 0.8)
        self.nodes[1].add_property("bright_state_param", 0.7)

        heralded_connection = self.initialize_components()
        heralded_connection.port_A.connect(self.nodes[0].ports["to_heralded_connection"])
        heralded_connection.port_B.connect(self.nodes[1].ports["to_heralded_connection"])
        self.md = SingleClickMagicDistributor(nodes=self.nodes, heralded_connection=heralded_connection)

        assert self.md.fixed_delivery_parameters[0]["alpha_A"] == 0.8
        assert self.md.fixed_delivery_parameters[0]["alpha_B"] == 0.7

    def test_reading_different_alpha_from_node(self):
        """If nodes have dictionary with different bright-state parameters, SingleClickMD should read them."""
        self.nodes[0].add_ports(["ENT_A"])
        self.nodes[1].add_ports(["ENT_A"])

        self.nodes[0].add_property("bright_state_param", {"ENT_A": 0.1, "to_heralded_connection": 0.3})
        self.nodes[1].add_property("bright_state_param", {"ENT_A": 0.2, "to_heralded_connection": 0.4})

        heralded_connection = self.initialize_components()
        heralded_connection.port_A.connect(self.nodes[0].ports["ENT_A"])
        heralded_connection.port_B.connect(self.nodes[1].ports["ENT_A"])
        self.md = SingleClickMagicDistributor(nodes=self.nodes, heralded_connection=heralded_connection)
        assert self.md.fixed_delivery_parameters[0]["alpha_A"] == 0.1
        assert self.md.fixed_delivery_parameters[0]["alpha_B"] == 0.2


class TestDoubleClickMagicDistributor(unittest.TestCase):

    class EmissionProcessor(QuantumProcessor):
        """Quantum processor with the properties, instructions and attributes required to read off properties."""

        def __init__(self, name, emission_fidelity, emission_duration, collection_efficiency, num_multiplexing_modes):
            super().__init__(name=name, num_positions=2)
            self.add_property("emission_fidelity", value=emission_fidelity)
            self.add_property("collection_efficiency", value=collection_efficiency)
            self.add_property("num_multiplexing_modes", value=num_multiplexing_modes)
            self.add_instruction(instruction=INSTR_INIT, duration=0)
            self.add_instruction(instruction=INSTR_EMIT, duration=emission_duration)
            self.emission_position = 1

    def message_handler_factory(self, node):
        """Function that returns message handler functions. Used to check if delivered messages meet expectations."""
        def message_handler(message):

            # test the message
            assert message is not None
            assert isinstance(message.items[0], BSMOutcome)
            assert message.items[0].success
            BellIndex(message.items[0].bell_index)  # check if it is a valid bell index (will raise error if not)
            assert message.meta["id"] == "qdetector"
            if node.name == "Alice":
                side = "A"
            elif node.name == "Bob":
                side = "B"
            else:
                raise RuntimeError("Unknown node.")

            # register successfully received message
            self.number_of_messages[side] += 1

            # check if message was obtained at correct time
            end_time_of_cycle = ns.sim_time() - self.travel_duration[side] + max(self.travel_duration.values())
            num_cycles = round(end_time_of_cycle / self.expected_cycle_time)
            # deviations of order 1E-10 * waiting time are introduced by magic distributor
            assert np.isclose(num_cycles * self.expected_cycle_time, end_time_of_cycle, rtol=1E-9)

            # uncomment this print statement for extra debug info
            # print(f"\n\nsim time: {ns.sim_time()}\n\n"
            #       f"end_time_of_cycle: {end_time_of_cycle}\n\n"
            #       f"cycle_time: {self.expected_cycle_time}\n\n"
            #       f"ratio: {end_time_of_cycle / self.expected_cycle_time}\n\n"
            #       f"md state delay:{self.md.fixed_delivery_parameters[0]['state_delay'][node.ID]}\n\n"
            #       f"md label delay:{self.md.fixed_delivery_parameters[0]['label_delay'][node.ID]}\n\n"
            #       f"travel time:{self.travel_duration}"
            #       f"\n\naccuracy: "
            #       f"{abs(end_time_of_cycle - num_cycles * self.expected_cycle_time) / end_time_of_cycle}\n\n")

        return message_handler

    def setUp(self):
        ns.sim_reset()
        self.length = {"A": 10, "B": 30}
        self.speed_of_light = {"A": 100, "B": 50}
        self.emission_duration = {"A": 1000, "B": 10000}
        self.emission_fidelity = {"A": .8, "B": .9}
        self.collection_efficiency = {"A": .2, "B": .5}
        self.p_loss_init = {"A": .95, "B": .90}
        self.p_loss_length = {"A": .25, "B": .22}
        self.dark_count_probability = .01
        self.detector_efficiency = .9
        self.visibility = 1
        self.num_resolving = False
        self.num_multiplexing_modes = 10
        self.coin_prob_ph_ph = 0.1
        self.coin_prob_ph_dc = 0.2
        self.coin_prob_dc_dc = 0.3

        self.nodes = {"A": Node("Alice"), "B": Node("Bob")}

        for side, node in self.nodes.items():
            node.add_ports(["to_heralded_connection"])
            node.qmemory = self.EmissionProcessor(name=f"test_processor_{node.name}",
                                                  emission_fidelity=self.emission_fidelity[side],
                                                  emission_duration=self.emission_duration[side],
                                                  collection_efficiency=self.collection_efficiency[side],
                                                  num_multiplexing_modes=self.num_multiplexing_modes)

        self.number_of_messages = {"A": 0, "B": 0}

    @property
    def travel_duration(self):
        """Time it takes to travel from a node to the midpoint."""
        return {side: self.length[side] / self.speed_of_light[side] * 1E9 for side in ["A", "B"]}

    @property
    def expected_cycle_time(self):
        """Expected time a single cycle of heralded entanglement generation takes."""
        return max([2 * self.travel_duration[side] + self.emission_duration[side]
                    for side in ["A", "B"]])

    def assert_parameters_correct(self):
        """Check if magic distributor has set its internal parameter values correctly"""

        for side in self.md.SIDES:
            assert self.md.fixed_delivery_parameters[0][f"emission_fidelity_{side}"] == self.emission_fidelity[side]
            probability_photon_couples_to_fiber = self.collection_efficiency[side] * (1 - self.p_loss_init[side])
            assert np.isclose(self.md.fixed_delivery_parameters[0][f"p_loss_init_{side}"],
                              1 - probability_photon_couples_to_fiber)
            assert self.md.fixed_delivery_parameters[0][f"emission_duration_{side}"] == self.emission_duration[side]
            assert self.md.fixed_delivery_parameters[0][f"speed_of_light_{side}"] == self.speed_of_light[side]
            assert self.md.fixed_delivery_parameters[0]["dark_count_probability"] == self.dark_count_probability
            assert self.md.fixed_delivery_parameters[0]["detector_efficiency"] == self.detector_efficiency
            assert self.md.fixed_delivery_parameters[0]["visibility"] == self.visibility
            assert self.md.fixed_delivery_parameters[0]["num_multiplexing_modes"] == self.num_multiplexing_modes
            assert self.md.fixed_delivery_parameters[0]["coin_prob_ph_ph"] == self.coin_prob_ph_ph
            assert self.md.fixed_delivery_parameters[0]["coin_prob_ph_dc"] == self.coin_prob_ph_dc
            assert self.md.fixed_delivery_parameters[0]["coin_prob_dc_dc"] == self.coin_prob_dc_dc

        bsm_time = max([self.emission_duration[side] + self.travel_duration[side] for side in ["A", "B"]])
        for side, node in self.nodes.items():
            assert self.md.fixed_delivery_parameters[0]["state_delay"][node.ID] == bsm_time - self.travel_duration[side]
            assert self.md.fixed_delivery_parameters[0]["label_delay"][node.ID] == 2 * self.travel_duration[side]
        assert self.md.fixed_delivery_parameters[0]["cycle_time"] == self.expected_cycle_time

    def assert_md_works(self):
        """Check that created magic distributor works as expected."""
        for side, node in self.nodes.items():
            port = node.ports["to_heralded_connection"]
            port.bind_input_handler(self.message_handler_factory(node=node))
            port.connect(self.heralded_connection.ports[side])
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                              heralded_connection=self.heralded_connection)
        self.md.add_delivery(memory_positions={node.ID: 0 for node in self.nodes.values()},
                             ports={node.ID: "to_heralded_connection" for node in self.nodes.values()})  # TODO don't add ports?
        ns.logger.setLevel(logging.DEBUG)
        ns.sim_run()
        assert ns.sim_time() > 1
        assert self.number_of_messages == {"A": 1, "B": 1}

    def test_passing_parameters_manually(self):
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()), heralded_connection=None,
                                              cycle_time=None,
                                              length_A=self.length["A"],
                                              length_B=self.length["B"],
                                              p_loss_init_A=1 - ((1 - self.p_loss_init["A"]) *
                                                                 self.collection_efficiency["A"]),
                                              p_loss_init_B=1 - ((1 - self.p_loss_init["B"]) *
                                                                 self.collection_efficiency["B"]),
                                              p_loss_length_A=self.p_loss_length["A"],
                                              p_loss_length_B=self.p_loss_length["B"],
                                              speed_of_light_A=self.speed_of_light["A"],
                                              speed_of_light_B=self.speed_of_light["B"],
                                              dark_count_probability=self.dark_count_probability,
                                              detector_efficiency=self.detector_efficiency,
                                              visibility=self.visibility, num_resolving=self.num_resolving,
                                              emission_duration_A=self.emission_duration["A"],
                                              emission_duration_B=self.emission_duration["B"],
                                              emission_fidelity_A=self.emission_fidelity["A"],
                                              emission_fidelity_B=self.emission_fidelity["B"],
                                              num_multiplexing_modes=self.num_multiplexing_modes,
                                              coin_prob_ph_ph=self.coin_prob_ph_ph,
                                              coin_prob_ph_dc=self.coin_prob_ph_dc,
                                              coin_prob_dc_dc=self.coin_prob_dc_dc
                                              )
        self.assert_parameters_correct()

    def test_default_parameters(self):
        for node in self.nodes.values():
            node.qmemory = QuantumProcessor(name=node.name)  # otherwise, some parameters are obtained from the qmemory
        with self.assertWarns(Warning):
            self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                                  length_A=self.length["A"],
                                                  length_B=self.length["B"])
        for param_name, default_value in self.md.DEFAULT_VALUES.items():
            assert self.md.fixed_delivery_parameters[0][param_name] == default_value

    def test_overwriting_component_parameters(self):
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                              heralded_connection=MiddleHeraldedConnection(name="test_MHC", length=10),
                                              p_loss_init_A=.3, detector_efficiency=.6, coin_prob_ph_ph=0.5)
        assert self.md.fixed_delivery_parameters[0]["p_loss_init_A"] == .3
        assert self.md.fixed_delivery_parameters[0]["detector_efficiency"] == .6
        assert self.md.fixed_delivery_parameters[0]["coin_prob_ph_ph"] == 0.5

    def test_length_not_specified(self):
        with self.assertRaises(ValueError):
            self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()), length_A=10)

    def test_nonunit_visibility(self):
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()), length_A=5, length_B=5,
                                              visibility=.8)
        assert self.md.fixed_delivery_parameters[0]["visibility"] == .8
        self.md.add_delivery(memory_positions={node.ID: 0 for node in self.nodes.values()})
        ns.sim_run()

    def test_number_resolving(self):
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()), length_A=5, length_B=5,
                                              num_resolving=True)
        assert self.md.fixed_delivery_parameters[0]["num_resolving"]
        self.md.add_delivery(memory_positions={node.ID: 0 for node in self.nodes.values()})
        ns.sim_run()

    def test_nonunit_collection_efficiency(self):
        """No p_loss_init from component, but nonunit collection efficiency; check they are set correctly."""
        md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()), length_A=5, length_B=5)
        assert np.isclose(md.fixed_delivery_parameters[0]["p_loss_init_A"], 1 - self.collection_efficiency["A"])
        assert np.isclose(md.fixed_delivery_parameters[0]["p_loss_init_B"], 1 - self.collection_efficiency["B"])

    def test_symmetric(self):
        self.length["B"] = self.length["A"]
        self.speed_of_light["B"] = self.speed_of_light["A"]
        self.p_loss_init["B"] = self.p_loss_init["A"]
        self.p_loss_length["B"] = self.p_loss_length["A"]
        self.heralded_connection = MiddleHeraldedConnection(name="test_heralded_connection_symmetric",
                                                            length=self.length["A"] * 2,
                                                            speed_of_light=self.speed_of_light["A"],
                                                            p_loss_init=self.p_loss_init["A"],
                                                            p_loss_length=self.p_loss_length["A"],
                                                            dark_count_probability=self.dark_count_probability,
                                                            detector_efficiency=self.detector_efficiency,
                                                            visibility=self.visibility,
                                                            num_resolving=self.num_resolving,
                                                            coin_prob_ph_ph=self.coin_prob_ph_ph,
                                                            coin_prob_ph_dc=self.coin_prob_ph_dc,
                                                            coin_prob_dc_dc=self.coin_prob_dc_dc)
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                              heralded_connection=self.heralded_connection)
        self.assert_md_works()

    def test_same_channels(self):
        self.speed_of_light["B"] = self.speed_of_light["A"]
        self.p_loss_init["B"] = self.p_loss_init["A"]
        self.p_loss_length["B"] = self.p_loss_length["A"]
        self.heralded_connection = SameChannelsHeraldedConnection(name="test_heralded_connection_symmetric",
                                                                  length_A=self.length["A"],
                                                                  length_B=self.length["B"],
                                                                  speed_of_light=self.speed_of_light["A"],
                                                                  p_loss_init=self.p_loss_init["A"],
                                                                  p_loss_length=self.p_loss_length["A"],
                                                                  dark_count_probability=self.dark_count_probability,
                                                                  detector_efficiency=self.detector_efficiency,
                                                                  visibility=self.visibility,
                                                                  num_resolving=self.num_resolving,
                                                                  coin_prob_ph_ph=self.coin_prob_ph_ph,
                                                                  coin_prob_ph_dc=self.coin_prob_ph_dc,
                                                                  coin_prob_dc_dc=self.coin_prob_dc_dc)
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                              heralded_connection=self.heralded_connection)
        self.assert_md_works()

    def test_asymmetric(self):
        self.heralded_connection = HeraldedConnection(name="test_heralded_connection_symmetric",
                                                      length_A=self.length["A"],
                                                      length_B=self.length["B"],
                                                      speed_of_light_A=self.speed_of_light["A"],
                                                      speed_of_light_B=self.speed_of_light["B"],
                                                      p_loss_init_A=self.p_loss_init["A"],
                                                      p_loss_init_B=self.p_loss_init["B"],
                                                      p_loss_length_A=self.p_loss_length["A"],
                                                      p_loss_length_B=self.p_loss_length["B"],
                                                      dark_count_probability=self.dark_count_probability,
                                                      detector_efficiency=self.detector_efficiency,
                                                      visibility=self.visibility,
                                                      num_resolving=self.num_resolving,
                                                      coin_prob_ph_ph=self.coin_prob_ph_ph,
                                                      coin_prob_ph_dc=self.coin_prob_ph_dc,
                                                      coin_prob_dc_dc=self.coin_prob_dc_dc
                                                      )
        self.md = DoubleClickMagicDistributor(nodes=list(self.nodes.values()),
                                              heralded_connection=self.heralded_connection)
        self.assert_parameters_correct()
        self.assert_md_works()


class TestDepolariseWithFailureMagicDistributor(unittest.TestCase):
    def test_parameter_passing(self):
        nodes = [Node("Alice"), Node("Bob")]
        for node in nodes:
            node.qmemory = QuantumProcessor(name=f"test_processor_{node.name}")
        prob_max_mixed = 0.25
        prob_success = 0.1
        depolarise_magic_distributor = DepolariseWithFailureMagicDistributor(nodes, prob_max_mixed, prob_success)
        assert depolarise_magic_distributor.prob_max_mixed == prob_max_mixed
        assert depolarise_magic_distributor.prob_success == prob_success


if __name__ == "__main__":
    unittest.main()

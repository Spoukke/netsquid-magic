import pytest
import random

import netsquid as ns
from pydynaa import EventExpression, EventType
from netsquid.protocols.protocol import Protocol

from netsquid_magic.sleeper import Sleeper


class SleepProtocol(Protocol):
    def __init__(self, times):
        super().__init__()
        self._sleeper = Sleeper()
        self._times = times
        self._sleep_times = []

    def run(self):
        for _ in range(self._times):
            yield self._sleeper.sleep()
            self._sleep_times.append(ns.sim_time())


class RandomEventProtocol(Protocol):
    def __init__(self, times):
        super().__init__()
        self._times = times
        self._ev = EventType('TEST', 'test')
        self._sleep_times = []

    def run(self):
        for _ in range(self._times):
            delay = random.randint(0, 100)
            self._sleep_times.append(ns.sim_time() + delay)
            self._schedule_after(delay, self._ev)
            yield EventExpression(source=self, event_type=self._ev)


@pytest.mark.parametrize('times', [
    1,
    2,
    10,
    83,
])
def test_no_event(times):
    ns.sim_reset()
    sleep_protocol = SleepProtocol(times)
    sleep_protocol.start()
    ns.sim_run()
    assert times == ns.sim_time()
    assert sleep_protocol._sleep_times == list(range(1, times + 1))


@pytest.mark.parametrize('times', [
    1,
    2,
    10,
    83,
])
def test_other_events(times):
    ns.sim_reset()
    random_event_protocol = RandomEventProtocol(times)
    random_event_protocol.start()
    sleep_protocol = SleepProtocol(times)
    sleep_protocol.start()
    ns.sim_run()
    assert sleep_protocol._sleep_times == random_event_protocol._sleep_times
